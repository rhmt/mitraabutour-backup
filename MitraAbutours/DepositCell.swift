//
//  DepositCell.swift
//  MitraAbutours
//
//  Created by Caryta on 10/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class DepositCell: UITableViewCell {
    
    @IBOutlet weak var viewTanggal: UIView!
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblBulanTahun: UILabel!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblKodeTransaksi: UILabel!
    @IBOutlet weak var lblWaktu: UILabel!
    @IBOutlet weak var lblNominal: UILabel!
    @IBOutlet weak var lblKeterangan: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
