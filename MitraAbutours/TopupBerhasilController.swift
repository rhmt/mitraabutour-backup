//
//  TopupBerhasilController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class TopupBerhasilController: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnOk: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign() {
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.viewMain.layer.cornerRadius = 5
        self.btnOk.layer.cornerRadius = 5
        self.showAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }, completion: {(finished: Bool) in
            if(finished){
                self.view.removeFromSuperview()
            }
        })
    }
    
    @IBAction func btnOkTapped(_ sender: UIButton) {
        self.removeAnimate()
    }
}
