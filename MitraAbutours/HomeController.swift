//
//  HomeController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/15/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Auk

class HomeController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var viewSubNavBar: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var scrollBanner: UIScrollView!
    @IBOutlet weak var pageBanner: UIPageControl!
    @IBOutlet weak var imgArrowRightBanner: UIImageView!
    @IBOutlet weak var imgArrowLeftBanner: UIImageView!
    @IBOutlet weak var segmentMenu: UIView!
    @IBOutlet weak var colMenu: UICollectionView!
    
    var dataMenu = [String]()
    var dataImgMenu = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollBanner.delegate = self
        
        self.setDesign()
        
        let menu = ["Beli Paket", "Topup", "Transaksi", "Poin", "Voucher", "Transaksi Seat", "e-Voucher", "Help Center", "Pengaturan"]
        self.dataMenu = menu
        let imgMenu = ["beli_paket", "topup", "transaksi", "poin", "voucher", "transfer_seat", "deposit", "help_center", "setting"]
        self.dataImgMenu = imgMenu
        self.colMenu.delegate = self
        self.colMenu.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        //Banner
        self.scrollBanner.auk.settings.contentMode = .scaleToFill
        self.scrollBanner.auk.settings.pageControl.visible = false
        self.scrollBanner.auk.show(image: UIImage(named: "testing_banner1")!)
        self.scrollBanner.auk.show(image: UIImage(named: "testing_banner2")!)
        self.scrollBanner.auk.startAutoScroll(delaySeconds: 10)
        //Page Banner
        self.pageBanner.numberOfPages = self.scrollBanner.auk.images.count
        //Scroll Banner
        let next = UITapGestureRecognizer(target: self, action: #selector(self.toNextBanner(_:)))
        self.imgArrowRightBanner.isUserInteractionEnabled = true
        self.imgArrowRightBanner.addGestureRecognizer(next)
        let previous = UITapGestureRecognizer(target: self, action: #selector(self.toPreviousBanner(_:)))
        self.imgArrowLeftBanner.isUserInteractionEnabled = true
        self.imgArrowLeftBanner.addGestureRecognizer(previous)        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.pageBanner.currentPage = self.scrollBanner.auk.currentPageIndex!
    }
    
    func toNextBanner(_ sender: UITapGestureRecognizer){
        self.scrollBanner.auk.scrollToNextPage()
        self.pageBanner.currentPage = self.scrollBanner.auk.currentPageIndex!
    }
    
    func toPreviousBanner(_ sender: UITapGestureRecognizer){
        self.scrollBanner.auk.scrollToPreviousPage()
        self.pageBanner.currentPage = self.scrollBanner.auk.currentPageIndex!
    }
    
    @IBAction func btnPulsaTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnListrikTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnPaketData(_ sender: UIButton) {
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dataCell", for: indexPath) as! ProfileCell
        
        cell.viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        cell.lblMenu.text = self.dataMenu[indexPath.row]
        cell.imgMenu.image = UIImage(named: self.dataImgMenu[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeCell:CGFloat = (UIScreen.main.bounds.size.width - 80) / 3
        return CGSize(width: sizeCell, height: sizeCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.dataMenu[indexPath.row] {
        case "Beli Paket":
            self.performSegue(withIdentifier: "showPaketUmrah", sender: self)
        default:
            return
        }
    }
}
