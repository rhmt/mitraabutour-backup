//
//  CariPaketUmrahController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import DropDown
import Alamofire
import SwiftyJSON
import RealmSwift

class CariPaketUmrahController: UIViewController{
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var txtKota: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPaket: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTahun: SkyFloatingLabelTextField!
    @IBOutlet weak var txtBulan: SkyFloatingLabelTextField!
    @IBOutlet weak var btnCari: UIButton!
    
    var alertLoading: UIAlertController!
    let listKota = DropDown()
    let listPaket = DropDown()
    let listTahun = DropDown()
    let listBulan = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listKota,
            self.listPaket,
            self.listTahun,
            self.listBulan
        ] }()
    var dataKdKota = [String]( )
    var dataKota = [String]()
    var kdKotaSelected = ""
    var dataKotaShow = [String]()
    var kotaShowSelected = "Semua Kota"
    var dataJenisPaket = [String]()
    var paketSelected = ""
    var dataTahun = [String]()
    var tahunSelected = ""
    var dataBulan = [String]()
    var bulanSelected = ""
    var dataCariPaket:JSON!
    var titleError = ""
    var messageError = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        self.btnCari.layer.cornerRadius = 5
        
        //Set Data Dropdown
        self.getListKota()
        self.dataJenisPaket = ["Semua Paket", "Promo", "Reguler", "Priority"]
        let currentYear = Calendar.current.component(.year, from: Date())
        self.dataTahun.append("Semua Tahun")
        for i in 0..<3 {
            self.dataTahun.append(String(currentYear+i))
        }
        self.dataBulan = ["Semua Bulan", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        self.setupListPaket()
        self.setupListTahun()
        self.setupListBulan()
    }
    
    func getListKota(){
        let url = "\(Helper.baseUrl)v1/kantor"
        let headers = [
            "Accept" : "application/json",
            "x-zona" : "",
            "x-per-page" : "",
            "x-page" : ""
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)["data"]
                    self.dataKdKota.removeAll()
                    self.dataKota.removeAll()
                    self.dataKdKota.append("")
                    self.dataKota.append("Semua Kota")
                    for i in 0..<jason.count{
                        let kd = jason[i]["kdkantor"].stringValue
                        self.dataKdKota.append(kd)
                        let nama = jason[i]["namakantor"].stringValue + ", " + jason[i]["kota"].stringValue
                        self.dataKota.append(nama.lowercased())
                        self.dataKotaShow.append(jason[i]["kota"].stringValue.lowercased())
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListKota()
        }
    }
    
    func setupListKota(){
        self.listKota.anchorView = self.txtKota
        self.listKota.bottomOffset = CGPoint(x: 0, y: self.txtKota.bounds.height)
        self.listKota.direction = .bottom
        self.listKota.dataSource = self.dataKota
        self.listKota.selectionAction = { [unowned self] (index, item) in
            self.txtKota.text = item
            let data = self.dataKota.index(of: item)
            let kd = self.dataKdKota[data!]
            self.kdKotaSelected = kd
            let kotaShow = self.dataKotaShow[data!]
            self.kotaShowSelected = kotaShow
            if item == "Semua Kota" {
                self.kdKotaSelected = ""
                self.kotaShowSelected = "Semua Kota"
            }
        }
    }
    func setupListPaket(){
        self.listPaket.anchorView = self.txtPaket
        self.listPaket.bottomOffset = CGPoint(x: 0, y: self.txtPaket.bounds.height)
        self.listPaket.direction = .bottom
        self.listPaket.dataSource = self.dataJenisPaket
        self.listPaket.selectionAction = { [unowned self] (index, item) in
            self.txtPaket.text = item
            //self.paketSelected = item
            if item == "Semua Paket" {
                self.paketSelected = ""
            }
        }
    }
    func setupListTahun(){
        self.listTahun.anchorView = self.txtTahun
        self.listTahun.bottomOffset = CGPoint(x: 0, y: self.txtTahun.bounds.height)
        self.listTahun.direction = .bottom
        self.listTahun.dataSource = self.dataTahun
        self.listTahun.selectionAction = { [unowned self] (index, item) in
            self.txtTahun.text = item
            self.tahunSelected = item
            if item == "Semua Tahun" {
                self.tahunSelected = ""
            }
        }
    }
    func setupListBulan(){
        self.listBulan.anchorView = self.txtBulan
        self.listBulan.bottomOffset = CGPoint(x: 0, y: self.txtBulan.bounds.height)
        self.listBulan.direction = .bottom
        self.listBulan.dataSource = self.dataBulan
        self.listBulan.selectionAction = { [unowned self] (index, item) in
            self.txtBulan.text = item
            let data = self.dataBulan.index(of: item)
            var dataStr = String(data!)
            if dataStr.characters.count == 1 {
                dataStr = "0\(dataStr)"
            }
            self.bulanSelected = dataStr
            if item == "Semua Bulan" {
                self.bulanSelected = ""
            }
        }
    }
    
    @IBAction func btnKotaTapped(_ sender: UIButton) {
        self.listKota.show()
    }
    
    @IBAction func btnPaketTapped(_ sender: UIButton) {
        self.listPaket.show()
    }
    
    @IBAction func btnTahunTapped(_ sender: UIButton) {
        self.listTahun.show()
    }
    
    @IBAction func btnBulanTapped(_ sender: UIButton) {
        self.listBulan.show()
    }
    
    @IBAction func btnCariTapped(_ sender: UIButton) {
        self.alertLoading = UIAlertController(title: "Sedang Mencari", message: "Silahkan tunggu ...", preferredStyle: UIAlertControllerStyle.alert)
        self.present(self.alertLoading, animated: true, completion: nil)
        self.cariPaketUmrah()
    }
    
    func cariPaketUmrah(){
        let url = "\(Helper.baseUrl)v1/produk/umrah/"
        var dataToken = tb_token()
        if let token = try! Realm().objects(tb_token.self).first {
            dataToken = token
        }
        let headers = [
            "x-kode-kantor" : self.kdKotaSelected,
            "x-jenis-paket" : self.paketSelected,
            "x-tahun" : self.tahunSelected,
            "x-bulan" : self.bulanSelected,
            "x-per-page" : "20",
            "x-page" : "1",
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        print(headers)
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    let data = jason["data"]
                    if data.count > 0 {
                        self.dataCariPaket = data
                        self.alertLoading.dismiss(animated: true, completion: self.toListPaketUmrah)
                    }else{
                        self.titleError = "Info"
                        self.messageError = jason["message"].stringValue
                        self.alertLoading.dismiss(animated: true, completion: self.msgError)
                    }
                }else{
                    self.titleError = "Error"
                    self.messageError = "Terjadi kesalahan, silahkan coba beberapa saat lagi"
                    self.alertLoading.dismiss(animated: true, completion: self.msgError)
                }
            }
    }
    
    func toListPaketUmrah(){
        self.performSegue(withIdentifier: "showListPaketUmrah", sender: self)
    }
    
    func msgError(){
        let alert = UIAlertController(title: self.titleError, message: self.messageError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showListPaketUmrah" {
            let conn = segue.destination as! ListPaketUmrahController
            conn.dataCariPaket = self.dataCariPaket
            let data = self.kotaShowSelected + " - " + self.txtPaket.text! + " - " + self.txtBulan.text! + " " + self.txtTahun.text!
            conn.dataFilter = data
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
