//
//  DepositController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import XMSegmentedControl

class DepositController: UIViewController, XMSegmentedControlDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var lblSaldoAbupay: UILabel!
    @IBOutlet weak var btnTopup: UIButton!
    @IBOutlet weak var viewSaldo: UIView!
    @IBOutlet weak var lblSaldoDeposit: UILabel!
    @IBOutlet weak var lblSaldoPriority: UILabel!
    @IBOutlet weak var lblSaldoBisaDigunakan: UILabel!
    @IBOutlet weak var segmentMenu: XMSegmentedControl!
    @IBOutlet weak var tbData: UITableView!
    
    var statusSegmentMenu = 0 //0:Riwayat, 1:Transaksi
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        //Corner Radius
        self.btnTopup.layer.cornerRadius = 5
        self.viewSaldo.layer.cornerRadius = 5
        //Segment
        self.segmentMenu.segmentTitle = ["Riwayat", "Transaksi"]
        self.segmentMenu.backgroundColor = UIColor.white
        self.segmentMenu.highlightColor = Helper.customColorGreen2
        self.segmentMenu.tint = UIColor.black
        self.segmentMenu.highlightTint = UIColor.white
        self.segmentMenu.delegate = self
        
        self.tbData.delegate = self
        self.tbData.dataSource = self
    }
    
    func xmSegmentedControl(_ xmSegmentedControl: XMSegmentedControl, selectedSegment: Int) {
        if xmSegmentedControl == self.segmentMenu {
            if selectedSegment == 0 {
                print("riwayat")
                self.statusSegmentMenu = 0
            }else{
                print("transaksi")
                self.statusSegmentMenu = 1
            }
        }
        self.tbData.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! DepositCell
        
        if self.statusSegmentMenu == 0 {
            cell.imgArrow.isHidden = false
            cell.lblStatus.isHidden = true
        }else{
            cell.imgArrow.isHidden = true
            cell.lblStatus.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.statusSegmentMenu == 0 {
            let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popupDetailTopup") as! PopupDetailTopupController
            self.addChildViewController(Popover)
            Popover.view.frame = self.view.frame
            self.view.addSubview(Popover.view)
            Popover.didMove(toParentViewController: self)
        }
    }
    
    @IBAction func btnTopupTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showTopup", sender: self)
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
