//
//  FormulirAkunController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/2/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import RealmSwift

class FormulirAkunController: UIViewController {
    
    @IBOutlet weak var txtNama: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtTelpon: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUlangPassword: UITextField!
    @IBOutlet weak var btnKonfirmasi: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    let dropdownJK = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.dropdownJK ] }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.btnKonfirmasi.layer.cornerRadius = 5
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnKonfirmasiTapped(_ sender: UIButton) {
        if self.txtNama.text! == "" || self.txtEmail.text! == "" || self.txtTelpon.text! == "" || self.txtPassword.text! == "" || self.txtUlangPassword.text! == "" {
            let alert = UIAlertController(title: "Info", message: "Silahkan Lengkapi Data Terlebih Dahulu", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.register()
        }
    }
    
    func register(){
        let url = "\(Helper.baseUrl)v1/users"
        let headers = [
            "x-handphone" : self.txtTelpon.text!,
            "x-email" : self.txtEmail.text!,
            "x-nama" : self.txtNama.text!,
            "x-password" : self.txtPassword.text!,
            "x-referral" : "",
            "x-tipe-user" : "CMS",
            "x-client-id" : Helper.clientId,
            "x-device-id" : Helper.uid,
            "x-app-version" : Helper.appVersion,
            "Accept" : "application/json"
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let data = jason["data"]
                    print(jason)
                    if jason["status"].intValue == 200 || jason["status"].intValue == 201 {
                        let modelUser = tb_user()
                        modelUser.kode_user = data["kode_user"].stringValue
                        modelUser.email = data["email"].stringValue
                        modelUser.telepon = data["telepon"].stringValue
                        modelUser.nama_depan = data["nama_depan"].stringValue
                        modelUser.nama_belakang = data["nama_belakang"].stringValue
                        modelUser.jk = data["jk"].stringValue
                        modelUser.ktp = data["ktp"].stringValue
                        modelUser.role_id = data["role_id"].intValue
                        modelUser.role = data["role"].stringValue
                        modelUser.saldo = data["saldo"].intValue
                        modelUser.fee = data["fee"].intValue
                        modelUser.poin = data["poin"].intValue
                        modelUser.seat = data["seat"].intValue
                        modelUser.kode_referal = data["kode_referal"].stringValue
                        DBHelper.insert(modelUser)
                        
                        self.performSegue(withIdentifier: "showOTP", sender: self)
                    }else{
                        print("request gagal")
                        let alert = UIAlertController(title: "Error", message: data["message"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
        
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOTP" {
            let conn = segue.destination as! KonfirmasiOTPController
            conn.from = 1
            conn.username = self.txtTelpon.text!
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
}
