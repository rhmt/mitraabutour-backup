//
//  ListPaketUmrahCell.swift
//  MitraAbutours
//
//  Created by Caryta on 10/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class ListPaketUmrahCell: UITableViewCell {
    
    @IBOutlet weak var imgUtama: UIImageView!
    @IBOutlet weak var lblNamaPaket: UILabel!
    @IBOutlet weak var lblKeterangan: UILabel!
    @IBOutlet weak var lblHarga: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
