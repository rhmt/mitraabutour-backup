//
//  DBHelper.swift
//  MitraAbutours
//
//  Created by Caryta on 9/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import RealmSwift

struct DBHelper {
    
    static func insert(_ obj: Object){
        try! Realm().write(){
            try! Realm().add(obj)
        }
    }
    
    static func getAllUser() -> [tb_user]{
        let objs: Results<tb_user> = {
            try! Realm().objects(tb_user.self)
        }()
        return Array(objs)
    }
    
    static func getAllToken() -> [tb_token]{
        let objs: Results<tb_token> = {
            try! Realm().objects(tb_token.self)
        }()
        return Array(objs)
    }

}
