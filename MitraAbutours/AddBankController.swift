//
//  AddBankController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/18/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift
import SkyFloatingLabelTextField
import DropDown

class AddBankController: UIViewController {
    
    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var viewPilihBank: UIView!
    @IBOutlet weak var txtNama: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNoRek: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNamaBank: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var imgSeePassword: UIImageView!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var jsonEdit: JSON!
    var isEdit = 0
    let listBank = DropDown()
    lazy var dropDowns: [DropDown] = { return [ self.listBank ] }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setDesign()
        self.setupListBank()
        
        if self.isEdit == 1 {
            self.setDataEditBank()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNav.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNav.layer.shadowRadius = 1
        self.viewNav.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNav.layer.shadowOpacity = 0.5
        //Button
        self.btnSimpan.layer.cornerRadius = 5
        //Pilih Bank
        let bankTap = UITapGestureRecognizer(target: self, action: #selector(self.pilihBankTapped(_:)))
        self.viewPilihBank.isUserInteractionEnabled = true
        self.viewPilihBank.addGestureRecognizer(bankTap)
    }
    
    func setDataEditBank(){
        self.txtNamaBank.text = self.jsonEdit["nama_bank"].stringValue
        self.txtNoRek.text = self.jsonEdit["no_rekening"].stringValue
        self.txtNama.text = self.jsonEdit["atas_nama"].stringValue
    }
    
    func pilihBankTapped(_ sender: UITapGestureRecognizer){
        self.listBank.show()
    }
    
    func setupListBank(){
        let dataBank = [
        "BCA",
        "BCA SYARIAH",
        "BNI",
        "BNI SYARIAH",
        "BRI",
        "BRI SYARIAH",
        "BTN",
        "CIMB NIAGA",
        "MANDIRI",
        "MANDIRI SYARIAH",
        "MEGA",
        "OCBC NISP",
        "PERMATA",
        "LAINNYA"
        ]
        self.listBank.anchorView = self.txtNamaBank
        self.listBank.bottomOffset = CGPoint(x: 0, y: self.txtNamaBank.bounds.height)
        self.listBank.direction = .bottom
        self.listBank.dataSource = dataBank
        self.listBank.selectionAction = { [unowned self] (index, item) in
            self.txtNamaBank.text = item
        }
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
        if self.isEdit == 0 {
            self.addBank()
        } else {
            self.editBank()
        }
    }
    
    func addBank(){
        let url = "\(Helper.baseUrl)v1/user_bank"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "x-per-page" : "10",
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        let body = [
            "user_bank" : [
                "nama_bank" : self.txtNamaBank.text!,
                "no_rekening" : self.txtNoRek.text!,
                "atas_nama" : self.txtNama.text!,
                "cabang" : ""
            ]
        ]
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let message = jason["message"].stringValue
                    print(jason)
                    
                    if message == "OK" {
                        let alert = UIAlertController(title: "Info", message: "Data bank berhasil ditambahkan", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }        
    }
    
    func editBank(){
        let url = "\(Helper.baseUrl)v1/user_bank/\(self.jsonEdit["kode_bank"].stringValue)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        let body = [
            "user_bank" : [
                "nama_bank" : self.txtNamaBank.text!,
                "no_rekening" : self.txtNoRek.text!,
                "atas_nama" : self.txtNama.text!,
                "cabang" : ""
            ]
        ]
        Alamofire.request(url, method: .put, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let message = jason["message"].stringValue
                    print(jason)
                    
                    if message == "OK" {
                        let alert = UIAlertController(title: "Info", message: "Data bank berhasil ditambahkan", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
