//
//  GantiKataSandiController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/28/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import RealmSwift
import Alamofire
import SwiftyJSON

class GantiKataSandiController: UIViewController {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var txtKataSandiSekarang: SkyFloatingLabelTextField!
    @IBOutlet weak var txtKataSandiBaru: SkyFloatingLabelTextField!
    @IBOutlet weak var txtKonfirmKataSandi: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        //BtnSimpan
        self.btnSimpan.layer.cornerRadius = 5
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
        if self.txtKataSandiSekarang.text! == "" || self.txtKataSandiBaru.text! == "" || self.txtKonfirmKataSandi.text! == "" {
            let alert = UIAlertController(title: "Info", message: "Silahkan Lengkapi Data Terlebih Dahulu", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.gantiPassword()
        }
    }
    
    func gantiPassword(){
        let url = "\(Helper.baseUrl)v1/passwords/change"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)",
            "x-old-password" : self.txtKataSandiSekarang.text!,
            "x-password" : self.txtKataSandiBaru.text!,
            "x-password-confirmation" : self.txtKonfirmKataSandi.text!
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    let message = jason["message"].stringValue
                    let success = jason["success"].stringValue
                    if success == "true" {
                        let alert = UIAlertController(title: "Info", message: "Penggantian Kata Sandi Berhasil", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
