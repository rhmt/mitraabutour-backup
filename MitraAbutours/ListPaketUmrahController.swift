//
//  ListPaketUmrahController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class ListPaketUmrahController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var lblDataFilterPaket: UILabel!
    @IBOutlet weak var tbData: UITableView!
    
    var dataCariPaket:JSON!
    var dataFilter = ""
    var dataFilteredPaket: JSON!
    var bulanSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
        self.tbData.delegate = self
        self.tbData.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        self.lblDataFilterPaket.text = self.dataFilter
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataCariPaket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! ListPaketUmrahCell
        
        let data = self.dataCariPaket[indexPath.row]
        if let gambar = data["gambar"].string {
            cell.imgUtama.loadImageUsingCacheWithUrlString(gambar)
        }
        cell.lblNamaPaket.text = data["nama"].stringValue
        cell.lblKeterangan.text = "Keberangkatan " + data["kantor"].stringValue
        cell.lblHarga.text = "Rp. " + data["harga_jual"].stringValue
        
        return cell
    }    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.dataCariPaket[indexPath.row]
        let kode = data["kode_produk"].stringValue
        let kdKantor = data["kdkantor"].stringValue
        self.bulanSelected = data["bulan"].stringValue
        let tahun = data["tahun"].stringValue
        let url = "\(Helper.baseUrl)v1/produk/umrah/\(kode)"
        var dataToken = tb_token()
        if let token = try! Realm().objects(tb_token.self).first {
            dataToken = token
        }
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)",
            "x-kode-kantor" : kdKantor,
            "x-bulan" : self.bulanSelected,
            "x-tahun" : tahun
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)                    
                    if jason.count > 0 {
                        self.dataFilteredPaket = jason
                        self.performSegue(withIdentifier: "showDetailPaketUmrah", sender: self)
                    }else{
                        let alert = UIAlertController(title: "Info", message: jason["message"].stringValue, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailPaketUmrah" {
            let conn = segue.destination as! DetailPaketUmrahController
            conn.dataFilteredPaket = self.dataFilteredPaket
            conn.bulanSelected = self.bulanSelected
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
