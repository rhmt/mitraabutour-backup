//
//  AlamatController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/25/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift
import DropDown
import SkyFloatingLabelTextField

class AlamatController: UIViewController {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var txtNamaAlamat: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAtasNama: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNoTelpon: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProvinsi: UIView!
    @IBOutlet weak var txtProvinsi: SkyFloatingLabelTextField!
    @IBOutlet weak var viewKabKot: UIView!
    @IBOutlet weak var txtKabKot: SkyFloatingLabelTextField!
    @IBOutlet weak var viewKecamatan: UIView!
    @IBOutlet weak var txtKecamatan: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlamat: SkyFloatingLabelTextField!
    @IBOutlet weak var imgDefault: UIImageView!
    @IBOutlet weak var lblDefault: UILabel!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var isEdit = 0
    var jsonEdit: JSON!
    var statusDefault = false
    let listProvinsi = DropDown()
    let listKabKot = DropDown()
    let listKecamatan = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listProvinsi,
            self.listKabKot,
            self.listKecamatan
        ] }()
    var idProvinsiSelected = 0
    var dataIdProvinsi = [Int]()
    var dataProvinsi = [String]()
    var idKabKotSelected = 0
    var dataIdKabKot = [Int]()
    var dataKabKot = [String]()
    var idKecamatanSelected = 0
    var dataIdKecamatan = [Int]()
    var dataKecamatan = [String]()
    var latSelected = ""
    var longSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setDesign()
        self.setupTapGesture()
        self.getProvinsi()
        
        if self.isEdit == 1 {
            self.setDataEditAlamat()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        self.btnSimpan.layer.cornerRadius = 5
    }
    
    func setDataEditAlamat(){
        self.txtNamaAlamat.text = self.jsonEdit["nama_alamat"].stringValue
        self.txtAtasNama.text = self.jsonEdit["atas_nama"].stringValue
        self.txtNoTelpon.text = self.jsonEdit["telepon"].stringValue
        self.txtAlamat.text = self.jsonEdit["alamat"].stringValue
    }
    
    func setupTapGesture(){
        //Click to Check
        let tapCheck = UITapGestureRecognizer(target: self, action: #selector(self.checkLblTapped(_:)))
        tapCheck.numberOfTapsRequired = 1
        self.lblDefault.isUserInteractionEnabled = true
        self.lblDefault.addGestureRecognizer(tapCheck)
        let tapCheck2 = UITapGestureRecognizer(target: self, action: #selector(self.checkTapped(_:)))
        tapCheck2.numberOfTapsRequired = 1
        self.imgDefault.isUserInteractionEnabled = true
        self.imgDefault.addGestureRecognizer(tapCheck2)
        //Alamat
        let provinsiTap = UITapGestureRecognizer(target: self, action: #selector(self.provinsiTapped(_:)))
        self.viewProvinsi.isUserInteractionEnabled = true
        self.viewProvinsi.addGestureRecognizer(provinsiTap)
        let kabkotTap = UITapGestureRecognizer(target: self, action: #selector(self.kabkotTapped(_:)))
        self.viewKabKot.isUserInteractionEnabled = true
        self.viewKabKot.addGestureRecognizer(kabkotTap)
        let kecamatanTap = UITapGestureRecognizer(target: self, action: #selector(self.kecamatanTapped(_:)))
        self.viewKecamatan.isUserInteractionEnabled = true
        self.viewKecamatan.addGestureRecognizer(kecamatanTap)
    }
    
    func checkTapped(_ sender: UITapGestureRecognizer){
        self.agreeTapped()
    }
    func checkLblTapped(_ sender: UITapGestureRecognizer){
        self.agreeTapped()
    }
    
    func agreeTapped(){
        if self.statusDefault {
            self.statusDefault = false
            self.imgDefault.image = UIImage(named: "ico_unchecked")
        }else{
            self.statusDefault = true
            self.imgDefault.image = UIImage(named: "ico_checked")
        }
    }
    
    func provinsiTapped(_ sender: UITapGestureRecognizer){
        self.listProvinsi.show()
    }
    
    func kabkotTapped(_ sender: UITapGestureRecognizer){
        self.listKabKot.show()
    }
    
    func kecamatanTapped(_ sender: UITapGestureRecognizer){
        self.listKecamatan.show()
    }
    
    func setupListProvinsi(){
        self.listProvinsi.anchorView = self.txtProvinsi
        self.listProvinsi.bottomOffset = CGPoint(x: 0, y: self.txtProvinsi.bounds.height)
        self.listProvinsi.direction = .bottom
        self.listProvinsi.dataSource = self.dataProvinsi
        self.listProvinsi.selectionAction = { [unowned self] (index, item) in
            self.txtProvinsi.text = item
            let data = self.dataProvinsi.index(of: item)
            let id = self.dataIdProvinsi[data!]
            self.idProvinsiSelected = id
            self.getKabKotByProvinsi()
        }
    }
    
    func setupListKabKot(){
        self.listKabKot.anchorView = self.txtKabKot
        self.listKabKot.bottomOffset = CGPoint(x: 0, y: self.txtKabKot.bounds.height)
        self.listKabKot.direction = .bottom
        self.listKabKot.dataSource = self.dataKabKot
        self.listKabKot.selectionAction = { [unowned self] (index, item) in
            self.txtKabKot.text = item
            let data = self.dataKabKot.index(of: item)
            let id = self.dataIdKabKot[data!]
            self.idKabKotSelected = id
            self.getkecamtanByKabKot()
        }
    }
    
    func setupListKecamatan(){
        self.listKecamatan.anchorView = self.txtKecamatan
        self.listKecamatan.bottomOffset = CGPoint(x: 0, y: self.txtKecamatan.bounds.height)
        self.listKecamatan.direction = .bottom
        self.listKecamatan.dataSource = self.dataKecamatan
        self.listKecamatan.selectionAction = { [unowned self] (index, item) in
            self.txtKecamatan.text = item
            let data = self.dataKecamatan.index(of: item)
            let id = self.dataIdKecamatan[data!]
            self.idKecamatanSelected = id
        }
    }
    
    func getProvinsi(){
        let url = "\(Helper.baseUrl)v1/provinsi"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataProvinsi.removeAll()
                    self.dataIdProvinsi.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["province"].stringValue
                        self.dataProvinsi.append(data)
                        let id = jason[i]["province_id"].intValue
                        self.dataIdProvinsi.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListProvinsi()
        }
    }
    
    func getKabKotByProvinsi(){
        let url = "\(Helper.baseUrl)v1/kabkot/provinsi/\(self.idProvinsiSelected)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataKabKot.removeAll()
                    self.dataIdKabKot.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["city_name"].stringValue
                        let type = jason[i]["type"].stringValue
                        let kabkot = type + " " + data
                        self.dataKabKot.append(kabkot)
                        let id = jason[i]["city_id"].intValue
                        self.dataIdKabKot.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListKabKot()
        }
    }
    
    func getkecamtanByKabKot(){
        let url = "\(Helper.baseUrl)v1/kecamatan/kabkot/\(self.idKabKotSelected)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataKecamatan.removeAll()
                    self.dataIdKecamatan.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["subdistrict_name"].stringValue
                        self.dataKecamatan.append(data)
                        let id = jason[i]["subdistrict_id"].intValue
                        self.dataIdKecamatan.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListKecamatan()
        }
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
        if self.isEdit == 0 {
            self.addAlamat()
        } else {
            self.editAlamat()
        }
    }
    
    func addAlamat(){
        let url = "\(Helper.baseUrl)v1/alamat"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)",
            "Content-Type" : "application/json"
        ]
        let body:[String : Any] = [
            "nama_alamat" : self.txtNamaAlamat.text!,
            "atas_nama" : self.txtAtasNama.text!,
            "telepon" : self.txtNoTelpon.text!,
            "id_provinsi" : self.idProvinsiSelected,
            "id_kabkot" : self.idKabKotSelected,
            "id_kecamatan" : self.idKecamatanSelected,
            "alamat" : self.txtAlamat.text!,
            "latitude" : self.latSelected,
            "longitude" : self.longSelected
        ]
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let message = jason["message"].stringValue
                    print(jason)
                    
                    if message == "OK" {
                        let alert = UIAlertController(title: "Info", message: "Data alamat berhasil ditambahkan", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    func editAlamat(){
        let url = "\(Helper.baseUrl)v1/alamat/\(self.jsonEdit["id"].stringValue)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        let body:[String : Any] = [
            "nama_alamat" : self.txtNamaAlamat.text!,
            "atas_nama" : self.txtAtasNama.text!,
            "telepon" : self.txtNoTelpon.text!,
            "id_provinsi" : self.idProvinsiSelected,
            "id_kabkot" : self.idKabKotSelected,
            "id_kecamatan" : self.idKecamatanSelected,
            "alamat" : self.txtAlamat.text!,
            "latitude" : self.latSelected,
            "longitude" : self.longSelected
        ]
        Alamofire.request(url, method: .put, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let message = jason["message"].stringValue
                    print(jason)
                    
                    if message == "OK" {
                        let alert = UIAlertController(title: "Info", message: "Data alamat berhasil ditambahkan", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
