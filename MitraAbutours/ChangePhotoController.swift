//
//  ChangePhotoController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/18/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Photos

class ChangePhotoController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var viewPilihFoto: UIView!
    @IBOutlet weak var imgPilihFoto: UIImageView!
    @IBOutlet weak var viewFoto: UIView!
    @IBOutlet weak var imgFoto: UIImageView!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var viewPopup: UIView!
    
    let imgPicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.imgPicker.delegate = self
        self.btnSimpan.layer.cornerRadius = self.btnSimpan.frame.height / 2
        self.viewPilihFoto.isHidden = false
        self.viewFoto.isHidden = true
        self.viewPopup.isHidden = true
        
        let pilihPhoto = UITapGestureRecognizer(target: self, action: #selector(self.imgPilihFotoTapped(_:)))
        self.imgPilihFoto.isUserInteractionEnabled = true
        self.imgPilihFoto.addGestureRecognizer(pilihPhoto)
        let changePhoto = UITapGestureRecognizer(target: self, action: #selector(self.imgPilihFotoTapped(_:)))
        self.imgFoto.isUserInteractionEnabled = true
        self.imgFoto.addGestureRecognizer(changePhoto)
    }
    
    func imgPilihFotoTapped(_ sender: UITapGestureRecognizer){
        self.viewPopup.isHidden = false
    }
    
    @IBAction func btnAmbilGambarTapped(_ sender: UIButton) {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {            
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            //self.imgPicker.mediaTypes = [kUTTypeImage as String]
            self.imgPicker.allowsEditing = false
            self.present(self.imgPicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnPilihGaleryTapped(_ sender: UIButton) {
        self.imgPicker.allowsEditing = false
        self.imgPicker.sourceType = .photoLibrary
        present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        self.imgFoto.image = pickedImage
        
        self.viewPilihFoto.isHidden = true
        self.viewFoto.isHidden = false
        self.viewPopup.isHidden = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPilihPenyimpananLuarTapped(_ sender: UIButton) {
    }
    
    @IBAction func btnHapusTapped(_ sender: UIButton) {
        self.viewPilihFoto.isHidden = false
        self.viewFoto.isHidden = true
    }
    
    @IBAction func btnClosePopupTapped(_ sender: UIButton) {
        self.viewPopup.isHidden = true
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
    }
    
}
