//
//  DBMigration.swift
//  MitraAbutours
//
//  Created by Caryta on 9/22/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Realm

class DBMigration{
    
    static func migrationDeleteData(){ // Migrasi tapi semua data terhapus (alias reset dari awal)
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config
        try! Realm()
    }
    
    static func migration(){ // migrasi biasa
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                
                if oldSchemaVersion < 1 {
                    migration.enumerateObjects(ofType: tb_user.className()) { oldObject, newObject in
                        newObject?["data"] = ""
                    }    
                }
        }
        ) 
        Realm.Configuration.defaultConfiguration = config
        try! Realm()
    }
    
    static func increaseRealmVersion(){ // migrasi dengan menambahkan realm versi
        let config = Realm.Configuration(schemaVersion: try! schemaVersionAtURL(Realm.Configuration.defaultConfiguration.fileURL!) + 1)
        Realm.Configuration.defaultConfiguration = config        
        try! Realm()
    }
    
    static func toRecentSchemeRealmVersion(){
        let config = Realm.Configuration(schemaVersion: try! schemaVersionAtURL(Realm.Configuration.defaultConfiguration.fileURL!))
        Realm.Configuration.defaultConfiguration = config
        try! Realm()
    }
}
