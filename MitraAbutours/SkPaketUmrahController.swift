//
//  SkPaketUmrahController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SkPaketUmrahController: UIViewController{
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var segmentMenu: UISegmentedControl!
    @IBOutlet weak var tvData: UITextView!
    @IBOutlet weak var imgAgree: UIImageView!
    @IBOutlet weak var btnLanjut: UIButton!
    
    var dataPaket: JSON!
    var dataSk = [String]()
    var statusAgree = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        self.btnLanjut.layer.cornerRadius = 5
        
        self.dataSk.append(self.dataPaket["sk"].stringValue)
        self.dataSk.append(self.dataPaket["info_visa"].stringValue)
        self.selectMenuSegment()
    }
    
    @IBAction func segmentMenuSelected(_ sender: UISegmentedControl) {
        self.selectMenuSegment()
    }
    
    func selectMenuSegment(){
        let indexSegment = self.segmentMenu.selectedSegmentIndex
        self.tvData.text = self.dataSk[indexSegment]
    }
    
    @IBAction func btnAgreeTapped(_ sender: UIButton) {
        if self.statusAgree {
            self.statusAgree = false
            self.imgAgree.image = UIImage(named: "ico_unchecked")
        }else{
            self.statusAgree = true
            self.imgAgree.image = UIImage(named: "ico_checked")
        }
    }
    
    @IBAction func btnLanjutTapped(_ sender: UIButton) {
        if self.statusAgree {
            //self.performSegue(withIdentifier: "", sender: self)
        }else{
            let alert = UIAlertController(title: "Info", message: "Silahkan setujui dahulu", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
