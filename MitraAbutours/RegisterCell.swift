//
//  RegisterCell.swift
//  MitraAbutours
//
//  Created by Caryta on 8/31/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class RegisterCell: UITableViewCell {
    
    @IBOutlet weak var viewMain: UIView!    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var constLblSubTitle: NSLayoutConstraint!
    @IBOutlet weak var imgArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMain.layer.masksToBounds = false
        self.viewMain.layer.cornerRadius = 5
        self.viewMain.layer.shadowColor = Helper.customColorGreen2.cgColor
        self.viewMain.layer.shadowRadius = 2
        self.viewMain.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        self.viewMain.layer.shadowOpacity = 0.5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
