//
//  DBModel.swift
//  MitraAbutours
//
//  Created by Caryta on 9/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import RealmSwift

class intro: Object{
    dynamic var intro = 0
}

class tb_user: Object {
    //User
    dynamic var id = 0
    dynamic var kode_user = ""
    dynamic var email = ""
    dynamic var telepon = ""
    dynamic var nama_depan = ""
    dynamic var nama_belakang = ""
    dynamic var jk = ""
    dynamic var ktp = ""
    dynamic var role_id = 0
    dynamic var role = ""
    dynamic var nama_role = ""
    dynamic var saldo = 0
    dynamic var fee = 0
    dynamic var poin = 0
    dynamic var seat = 0
    dynamic var kode_referal = ""
    //Detail
    dynamic var kode_paket = ""
    dynamic var kode_kantor = ""
    dynamic var tempat_lahir = ""
    dynamic var tanggal_lahir = ""
    dynamic var nama_kerabat = ""
    dynamic var alamat_kerabat = ""
    dynamic var telepon_kerabat = ""
    dynamic var telepon_rumah = ""
    dynamic var id_provinsi = 0
    dynamic var id_kabkot = 0
    dynamic var id_kecamatan = 0
    dynamic var kelurahan = ""
    dynamic var alamat = ""
    dynamic var foto = ""
    dynamic var latitude = 0
    dynamic var longitude = 0
}

class tb_token: Object {
    dynamic var access_token = ""
    dynamic var token_type = ""
    dynamic var expires_in = 0
    dynamic var refresh_token = ""
}
