//
//  PopoverListController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/29/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class PopoverListController: UIViewController {
    
    var dari = 0 //1:Alamat, 2:Bank
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnUbahDataTapped(_ sender: UIButton) {
        if self.dari == 1 {
            let tmpController = self.presentingViewController as! ListAlamatController
            tmpController.isEdit = 1
            self.dismiss(animated: false) {
                tmpController.performSegue(withIdentifier: "showActionAlamat", sender: self)
            }
        } else {
            let tmpController = self.presentingViewController as! ListBankController
            tmpController.isEdit = 1
            self.dismiss(animated: false) {
                tmpController.performSegue(withIdentifier: "showActionBank", sender: self)
            }
        }
    }
    
    @IBAction func btnHapusTapped(_ sender: UIButton) {
        if self.dari == 1 {
            let tmpController = self.presentingViewController as! ListAlamatController
            self.dismiss(animated: false) {
                tmpController.deleteDataAlamat()
            }
        } else {
            let tmpController = self.presentingViewController as! ListBankController
            self.dismiss(animated: false) {
                tmpController.deleteDataBank()
            }
        }
    }
}
