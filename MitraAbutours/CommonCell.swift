//
//  CommonCell.swift
//  MitraAbutours
//
//  Created by Caryta on 9/20/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class CommonCell: UITableViewCell {
    
    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var imgData: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblKet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
