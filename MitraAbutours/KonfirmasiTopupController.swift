//
//  KonfirmasiTopupController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class KonfirmasiTopupController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var lblNoTransaksi: UILabel!
    @IBOutlet weak var lblMetode: UILabel!
    @IBOutlet weak var lblDariBank: UILabel!
    @IBOutlet weak var lblDariNoRekBank: UILabel!
    @IBOutlet weak var lblTujuanBank: UILabel!
    @IBOutlet weak var lblTujuanNoRekBank: UILabel!
    @IBOutlet weak var lblTotalTransfer: UILabel!
    @IBOutlet weak var viewImgBukti: UIView!
    @IBOutlet weak var imgBukti: UIImageView!
    @IBOutlet weak var lblTapImg: UILabel!
    @IBOutlet weak var btnLanjut: UIButton!
    
    let imgPicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        self.btnLanjut.layer.cornerRadius = 5
        self.imgPicker.delegate = self
    }
    
    @IBAction func btnImgBuktiTapped(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Upload Gambar", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Kamera", style: UIAlertActionStyle.default) { UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Galeri", style: UIAlertActionStyle.default) { UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Batal", style: UIAlertActionStyle.cancel) { UIAlertAction in }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func cameraTapped(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imgPicker.allowsEditing = false
            self.present(self.imgPicker, animated: true, completion: nil)
        } else {
            self.libraryTapped()
        }
    }
    
    func libraryTapped(){
        self.imgPicker.allowsEditing = false
        self.imgPicker.sourceType = .photoLibrary
        //self.imgPicker.mediaTypes = [kUTTypeImage as String]
        present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        self.viewImgBukti.backgroundColor = UIColor.white
        self.imgBukti.image = pickedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLanjutTapped(_ sender: UIButton) {
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popupBerhasilTopup") as! TopupBerhasilController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
