//
//  Helper.swift
//  MitraAbutours
//
//  Created by Caryta on 9/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import Alamofire
import SwiftyJSON
import RealmSwift

class Helper{
    static let baseUrl = "http://api2.abutours.com/"
    static let uid = UIDevice.current.identifierForVendor!.uuidString
    static let clientId = "a2c70490f91a379b87d93738fd9cb7e34e04f94567bd6161443ced212dc50d96"
    static let appVersion = "1"
    static let deviceName = UIDevice.current.modelName
    
    //Custom Color
    static let customColorGreen1 = UIColor(hex: "00D199")
    static let customColorGreen2 = UIColor(hex: "009776")
    static let customColorGreen3 = UIColor(hex: "03596D")
    static let customColorCoklat = UIColor(hex: "4B4B4B")
    static let customColorHitam = UIColor(hex: "141414")
    
    static func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    static func estimateFrameForText(_ text: String, width: Int) -> CGRect {
        let size = CGSize(width: width, height: 10000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
    }
    
    static func convertDate(_ data: String, from: String, to: String) -> String{
        let formatFrom = DateFormatter()
        formatFrom.dateFormat = from
        let formatTo = DateFormatter()
        formatTo.dateFormat = to
        
        let fromToDate = formatFrom.date(from: data)
        let result = formatTo.string(from: fromToDate!)
        
        return result
    }
    
    static func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory as NSString
    }
    
    static func backToLogin(_ view: UIViewController){
        //Clear data
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        //Data Intro di adain lagi
        let dataIntro = intro()
        dataIntro.intro = 1
        DBHelper.insert(dataIntro)
        
        let login = view.storyboard?.instantiateViewController(withIdentifier: "showLogin") as! LoginController
        view.present(login, animated: true, completion: nil)
    }
    
    static func tokenInfo(_ view: UIViewController){
        let url = "\(self.baseUrl)v1/oauth/token/info"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print("token info", jason)
                    if let _ = jason["error"].string {
                        self.tokenRefresh(view)
                    }
                }else{
                    print("Request Token Gagal")
                }
        }
    }
    
    static func tokenRefresh(_ view: UIViewController){
        let url = "\(self.baseUrl)/v1/oauth/token"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let params = [
            "grant_type" : "refresh_token",
            "refresh_token" : dataToken.refresh_token
        ]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print("token refresh", jason)
                    if let error = jason["error"].string {
                        let alert = UIAlertController(title: "Error", message: error + " \n Please login again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.backToLogin(view)
                        }))
                        view.present(alert, animated: true, completion: nil)
                    }else{
                        let modelToken = try! Realm().objects(tb_token.self).first!
                        try! Realm().write({ 
                            modelToken.access_token = jason["access_token"].stringValue
                            modelToken.token_type = jason["token_type"].stringValue
                            modelToken.expires_in = jason["expires_in"].intValue
                            modelToken.refresh_token = jason["access_token"].stringValue
                        })
                    }
                }else{
                    print("Request Refresh Token Gagal")
                }
        }
    }

}
