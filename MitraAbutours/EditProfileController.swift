//
//  EditProfileController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/16/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class EditProfileController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var tbMenu: UITableView!
    @IBOutlet weak var btnKeluar: UIButton!
    
    var dataMenu = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){        
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        //TbMenu
        let data = ["Edit Profil", "Alamat", "Rekening Bank", "Telepon", "Email", "Kata Sandi"]
        self.dataMenu = data
        self.tbMenu.delegate = self
        self.tbMenu.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! CommonCell
        
        cell.lblTitle.text = self.dataMenu[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "showEditDataProfile", sender: self)
        case 1:
            self.performSegue(withIdentifier: "showAlamat", sender: self)
        case 2:
            self.performSegue(withIdentifier: "showBank", sender: self)
        case 3:
            self.performSegue(withIdentifier: "showGantiTelpon", sender: self)
        case 4:
            self.performSegue(withIdentifier: "showGantiEmail", sender: self)
        case 5:
            self.performSegue(withIdentifier: "showGantiKataSandi", sender: self)
        default:
            return
        }
    }
    
    @IBAction func btnKeluarTapped(_ sender: UIButton) {
        self.logout()
    }
    
    func logout(){
        let url = "\(Helper.baseUrl)v1/oauth/revoke"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Basic MWNlYzI4NmM2YTQwNzgzNThlMTJmNTMyNGMzM2FhZWQ1NDg2MTcwYTk1MGVmODkzZjEzYjc4ZTA5NjE0MWQ2ZjoxYWRhYzc0MTQ4ZDliYjgwY2VkMTRhYzZjMzJhOTcwMDYyMTk5MTM4MzYzZjAyNDhlZDg5MTc2ZTQ4NzRlYjBj"
        ]
        let params = [
            "token" : dataToken.access_token
        ]
        Alamofire.request(url, method: .post, parameters: params, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    Helper.backToLogin(self)
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
