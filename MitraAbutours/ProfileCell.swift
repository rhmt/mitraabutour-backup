//
//  ProfileCell.swift
//  MitraAbutours
//
//  Created by Caryta on 9/12/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class ProfileCell: UICollectionViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imgMenu: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewContainer.layer.cornerRadius = 5        
        self.viewContainer.layer.borderWidth = 1
    }
}
