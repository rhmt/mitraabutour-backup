//
//  ListAlamatCell.swift
//  MitraAbutours
//
//  Created by Caryta on 9/27/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class ListAlamatCell: UITableViewCell {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblAlamat: UILabel!
    @IBOutlet weak var lblAtasNama: UILabel!
    @IBOutlet weak var imgDefaultAlamat: UIImageView!
    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewMain.layer.masksToBounds = false
        self.viewMain.clipsToBounds = true
        self.viewMain.layer.cornerRadius = 5
        self.viewMain.layer.borderColor = UIColor.lightGray.cgColor
        self.viewMain.layer.borderWidth = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
