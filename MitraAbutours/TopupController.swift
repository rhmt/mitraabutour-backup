//
//  TopupController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/4/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import RealmSwift
import DropDown

class TopupController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var txtNominal: SkyFloatingLabelTextField!
    @IBOutlet weak var imgRadioBank: UIImageView!
    @IBOutlet weak var viewBank: UIView!
    @IBOutlet weak var tbBankDari: UITableView!
    @IBOutlet weak var constTbBankDari: NSLayoutConstraint!
    @IBOutlet weak var tbBankKe: UITableView!
    @IBOutlet weak var constTbBankKe: NSLayoutConstraint!
    @IBOutlet weak var constViewBank: NSLayoutConstraint!
    @IBOutlet weak var imgRadioAbupay: UIImageView!
    @IBOutlet weak var viewKartuKredit: UIView!
    @IBOutlet weak var txtNoKartu: SkyFloatingLabelTextField!
    @IBOutlet weak var lblBulanCC: UILabel!
    @IBOutlet weak var lblTahunCC: UILabel!
    @IBOutlet weak var txtTigaDigitCC: UITextField!
    @IBOutlet weak var constViewKartuKredit: NSLayoutConstraint!
    @IBOutlet weak var btnLanjut: UIButton!
    
    var dataBankDari: JSON!
    var plusHeightTbBankDari = 0
    var dataBankKe: JSON!
    var plusHeightTbBankKe = 0
    let listBulan = DropDown()
    let listTahun = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.listBulan,
            self.listTahun
        ] }()
    var dataTahun = [String]()
    var tahunSelected = ""
    var dataBulan = [String]()
    var bulanSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        
        self.btnLanjut.layer.cornerRadius = 5
        //Data
        self.getListBankDari()
        self.getListBankKe()
        self.dataBulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
        let currentYear = Calendar.current.component(.year, from: Date())
        for i in 0..<10 {
            self.dataTahun.append(String(currentYear+i))
        }
        self.setupListBulan()
        self.setupListTahun()
        let tapBulan = UITapGestureRecognizer(target: self, action: #selector(self.bulanTapped(_:)))
        self.lblBulanCC.isUserInteractionEnabled = true
        self.lblBulanCC.addGestureRecognizer(tapBulan)
        let tapTahun = UITapGestureRecognizer(target: self, action: #selector(self.tahunTapped(_:)))
        self.lblTahunCC.isUserInteractionEnabled = true
        self.lblTahunCC.addGestureRecognizer(tapTahun)
    }
    
    func getListBankDari(){
        let url = "\(Helper.baseUrl)v1/user_bank"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "x-per-page" : "10",
            "Authorization" : "Bearer \(dataToken.access_token)",
            "Content-Type" : "application/json"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let data = jason["data"]
                    print(jason)
                    self.plusHeightTbBankDari = data.count * 50
                    self.constTbBankDari.constant = CGFloat(self.plusHeightTbBankDari)
                    self.dataBankDari = data
                    self.tbBankDari.delegate = self
                    self.tbBankDari.dataSource = self
                    self.tbBankDari.reloadData()
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
    }
    
    func getListBankKe(){
        let url = "\(Helper.baseUrl)v1/bank"
        Alamofire.request(url, method: .get)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let data = jason["data"]
                    print(jason)
                    self.plusHeightTbBankKe = data.count * 50
                    self.constTbBankKe.constant = CGFloat(self.plusHeightTbBankKe)
                    self.dataBankKe = data
                    self.tbBankKe.delegate = self
                    self.tbBankKe.dataSource = self
                    self.tbBankKe.reloadData()
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
    }
    
    @IBAction func btnRadioBankTapped(_ sender: UIButton) {
        self.imgRadioBank.image = UIImage(named: "ico_radio_on")
        self.imgRadioAbupay.image = UIImage(named: "ico_radio_off")
        UIView.animate(withDuration: 0.5, animations: {
            self.constViewBank.constant = CGFloat(50 + self.plusHeightTbBankDari + self.plusHeightTbBankKe)
            self.constViewKartuKredit.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tbBankDari {
            return self.dataBankDari.count
        } else {
            return self.dataBankKe.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! CommonCell
        
        if tableView == self.tbBankDari {
            let data = self.dataBankDari[indexPath.row]
            cell.lblTitle.text = data["nama_bank"].stringValue
            cell.lblSubTitle.text = data["no_rekening"].stringValue
            cell.lblKet.text = data["atas_nama"].stringValue
        } else {
            let data = self.dataBankKe[indexPath.row]
            cell.lblTitle.text = data["nama"].stringValue
            cell.lblSubTitle.text = data["norekening"].stringValue
            cell.lblKet.text = data["kdbank"].stringValue
        }
        
        return cell
    }
    
    @IBAction func btnRadioAbupayTapped(_ sender: UIButton) {
        self.imgRadioBank.image = UIImage(named: "ico_radio_off")
        self.imgRadioAbupay.image = UIImage(named: "ico_radio_on")
        UIView.animate(withDuration: 0.5, animations: {
            self.constViewBank.constant = 0
            self.constViewKartuKredit.constant = 125
            self.view.layoutIfNeeded()
        })
    }
    
    func setupListBulan(){
        self.listBulan.anchorView = self.lblBulanCC
        self.listBulan.bottomOffset = CGPoint(x: 0, y: self.lblBulanCC.bounds.height)
        self.listBulan.direction = .bottom
        self.listBulan.dataSource = self.dataBulan
        self.listBulan.selectionAction = { [unowned self] (index, item) in
            self.lblBulanCC.text = item
            self.lblBulanCC.textColor = UIColor.black
        }
    }
    func setupListTahun(){
        self.listTahun.anchorView = self.lblTahunCC
        self.listTahun.bottomOffset = CGPoint(x: 0, y: self.lblTahunCC.bounds.height)
        self.listTahun.direction = .bottom
        self.listTahun.dataSource = self.dataTahun
        self.listTahun.selectionAction = { [unowned self] (index, item) in
            self.lblTahunCC.text = item
            self.lblTahunCC.textColor = UIColor.black
            self.tahunSelected = item
        }
    }
    
    func bulanTapped(_ sender: UITapGestureRecognizer){
        self.listBulan.show()
    }
    
    func tahunTapped(_ sender: UITapGestureRecognizer){
        self.listTahun.show()
    }
    
    @IBAction func btnLanjutTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showKonfirmasiTopup", sender: self)
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
