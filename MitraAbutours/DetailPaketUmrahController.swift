//
//  DetailPaketUmrahController.swift
//  MitraAbutours
//
//  Created by Caryta on 10/5/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class DetailPaketUmrahController: UIViewController{
    
    @IBOutlet weak var imgBannerUtama: UIImageView!
    @IBOutlet weak var lblTipe: UILabel!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblKeterangan: UILabel!
    @IBOutlet weak var viewMainFasilitas: UIView!
    @IBOutlet weak var icoArrowFasilitas: UIImageView!
    @IBOutlet weak var constMainFasilitas: NSLayoutConstraint!
    @IBOutlet weak var viewSubFasilitas: UIView!
    @IBOutlet weak var constSubFasilitas: NSLayoutConstraint!
    @IBOutlet weak var viewMainItinerary: UIView!
    @IBOutlet weak var icoArrowItinerary: UIImageView!
    @IBOutlet weak var constMainItinerary: NSLayoutConstraint!
    @IBOutlet weak var viewSubItinerary: UIView!
    @IBOutlet weak var lblItinerary: UILabel!
    @IBOutlet weak var constSubItinerary: NSLayoutConstraint!
    @IBOutlet weak var viewMainRencana: UIView!
    @IBOutlet weak var icoArrowRencana: UIImageView!
    @IBOutlet weak var constMainRencana: NSLayoutConstraint!
    @IBOutlet weak var viewSubRencana: UIView!
    @IBOutlet weak var constSubRencana: NSLayoutConstraint!
    @IBOutlet weak var viewBulan1: UIView!
    @IBOutlet weak var lblBulan1: UILabel!
    @IBOutlet weak var viewBulan2: UIView!
    @IBOutlet weak var lblBulan2: UILabel!
    @IBOutlet weak var viewBulan3: UIView!
    @IBOutlet weak var lblBulan3: UILabel!
    @IBOutlet weak var viewBulan4: UIView!
    @IBOutlet weak var lblBulan4: UILabel!
    @IBOutlet weak var viewBulan5: UIView!
    @IBOutlet weak var lblBulan5: UILabel!
    @IBOutlet weak var viewBulan6: UIView!
    @IBOutlet weak var lblBulan6: UILabel!
    @IBOutlet weak var viewBulan7: UIView!
    @IBOutlet weak var lblBulan7: UILabel!
    @IBOutlet weak var viewBulan8: UIView!
    @IBOutlet weak var lblBulan8: UILabel!
    @IBOutlet weak var viewBulan9: UIView!
    @IBOutlet weak var lblBulan9: UILabel!
    @IBOutlet weak var viewBulan10: UIView!
    @IBOutlet weak var lblBulan10: UILabel!
    @IBOutlet weak var viewBulan11: UIView!
    @IBOutlet weak var lblBulan11: UILabel!
    @IBOutlet weak var viewBulan12: UIView!
    @IBOutlet weak var lblBulan12: UILabel!
    var viewBulans: [UIView] { return [
        self.viewBulan1, self.viewBulan2, self.viewBulan3, self.viewBulan4, self.viewBulan5, self.viewBulan6, self.viewBulan7, self.viewBulan8, self.viewBulan9, self.viewBulan10, self.viewBulan11, self.viewBulan12
    ]}
    var lblBulans: [UILabel] { return [
        self.lblBulan1, self.lblBulan2, self.lblBulan3, self.lblBulan4, self.lblBulan5, self.lblBulan6, self.lblBulan7, self.lblBulan8, self.lblBulan9, self.lblBulan10, self.lblBulan11, self.lblBulan12
        ]}
    @IBOutlet weak var viewMainTips: UIView!
    @IBOutlet weak var icoArrowTips: UIImageView!
    @IBOutlet weak var constMainTips: NSLayoutConstraint!
    @IBOutlet weak var viewSubTips: UIView!
    @IBOutlet weak var lblTips: UILabel!
    @IBOutlet weak var constSubTips: NSLayoutConstraint!
    @IBOutlet weak var lblHarga: UILabel!
    @IBOutlet weak var btnBeli: UIButton!
    
    var isFasilitas = false //True:kebuka, False:Ketutup
    var isItinerary = false //True:kebuka, False:Ketutup
    var isRencana = false //True:kebuka, False:Ketutup
    var isTips = false //True:kebuka, False:Ketutup
    var dataFilteredPaket: JSON!
    var bulanSelected = ""
    var dataIliteraryHeight:CGFloat = 0
    var dataTipsHeight:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showDetailUmrah()
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showDetailUmrah(){
        let data = self.dataFilteredPaket!
        if let gambar = data["gambar"].string {
            self.imgBannerUtama.loadImageUsingCacheWithUrlString(gambar)
        }
        self.lblTipe.text = data["sub_tipe"].stringValue
        self.lblNama.text = data["nama"].stringValue
        self.lblItinerary.text = data["itinerary"].stringValue
        self.lblHarga.text = "Rp. " + data["harga_jual"].stringValue
    }
    
    func setDesign(){
        self.btnBeli.layer.cornerRadius = 5
        self.dataIliteraryHeight = Helper.estimateFrameForText(self.dataFilteredPaket["itinerary"].stringValue, width: Int(UIScreen.main.bounds.size.width - 48)).height + 16
        for bulan in self.viewBulans {
            bulan.backgroundColor = UIColor.white
            bulan.layer.cornerRadius = 5
            bulan.layer.borderColor = Helper.customColorGreen2.cgColor
            bulan.layer.borderWidth = 1
        }
        for bulan in self.lblBulans {
            bulan.textColor = Helper.customColorGreen2
        }
        switch self.bulanSelected {
        case "01":
            self.viewBulan1.backgroundColor = Helper.customColorGreen2
            self.lblBulan1.textColor = UIColor.white
        case "02":
            self.viewBulan2.backgroundColor = Helper.customColorGreen2
            self.lblBulan2.textColor = UIColor.white
        case "03":
            self.viewBulan3.backgroundColor = Helper.customColorGreen2
            self.lblBulan3.textColor = UIColor.white
        case "04":
            self.viewBulan4.backgroundColor = Helper.customColorGreen2
            self.lblBulan4.textColor = UIColor.white
        case "05":
            self.viewBulan5.backgroundColor = Helper.customColorGreen2
            self.lblBulan5.textColor = UIColor.white
        case "06":
            self.viewBulan6.backgroundColor = Helper.customColorGreen2
            self.lblBulan6.textColor = UIColor.white
        case "07":
            self.viewBulan7.backgroundColor = Helper.customColorGreen2
            self.lblBulan7.textColor = UIColor.white
        case "08":
            self.viewBulan8.backgroundColor = Helper.customColorGreen2
            self.lblBulan8.textColor = UIColor.white
        case "09":
            self.viewBulan9.backgroundColor = Helper.customColorGreen2
            self.lblBulan9.textColor = UIColor.white
        case "10":
            self.viewBulan10.backgroundColor = Helper.customColorGreen2
            self.lblBulan10.textColor = UIColor.white
        case "11":
            self.viewBulan11.backgroundColor = Helper.customColorGreen2
            self.lblBulan11.textColor = UIColor.white
        case "12":
            self.viewBulan12.backgroundColor = Helper.customColorGreen2
            self.lblBulan12.textColor = UIColor.white
        default:
            return
        }
        self.dataTipsHeight = Helper.estimateFrameForText(self.lblTips.text!, width: Int(UIScreen.main.bounds.size.width - 48)).height + 16
    }
    
    @IBAction func btnFasilitasTapped(_ sender: UIButton) {
        if isFasilitas {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainFasilitas.constant = 42
                self.constSubFasilitas.constant = 0
                self.icoArrowFasilitas.transform = CGAffineTransform(rotationAngle: 0.0)
                self.isFasilitas = false
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainFasilitas.constant = 147
                self.constSubFasilitas.constant = 105
                self.icoArrowFasilitas.transform = CGAffineTransform(rotationAngle: 15.75)
                self.isFasilitas = true
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func btnIliteraryTapped(_ sender: UIButton) {
        if isItinerary {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainItinerary.constant = 41
                self.constSubItinerary.constant = 0
                self.icoArrowItinerary.transform = CGAffineTransform(rotationAngle: 0.0)
                self.isItinerary = false
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainItinerary.constant = 41 + self.dataIliteraryHeight
                self.constSubItinerary.constant = self.dataIliteraryHeight
                self.icoArrowItinerary.transform = CGAffineTransform(rotationAngle: 15.75)
                self.isItinerary = true
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func btnRencanaTapped(_ sender: UIButton) {
        if isRencana {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainRencana.constant = 41
                self.constSubRencana.constant = 0
                self.icoArrowRencana.transform = CGAffineTransform(rotationAngle: 0.0)
                self.isRencana = false
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainRencana.constant = 241
                self.constSubRencana.constant = 200
                self.icoArrowRencana.transform = CGAffineTransform(rotationAngle: 15.75)
                self.isRencana = true
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func btnTipsTapped(_ sender: UIButton) {
        if isTips {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainTips.constant = 41
                self.constSubTips.constant = 0
                self.icoArrowTips.transform = CGAffineTransform(rotationAngle: 0.0)
                self.isTips = false
                self.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.constMainTips.constant = 41 + self.dataTipsHeight
                self.constSubTips.constant = self.dataTipsHeight
                self.icoArrowTips.transform = CGAffineTransform(rotationAngle: 15.75)
                self.isTips = true
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func btnBeliTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showSkPaketUmrah", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSkPaketUmrah" {
            let conn = segue.destination as! SkPaketUmrahController
            conn.dataPaket = self.dataFilteredPaket
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
