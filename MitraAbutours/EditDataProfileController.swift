//
//  EditDataProfileController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/23/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift
import DropDown
import DatePickerDialog
import SkyFloatingLabelTextField

class EditDataProfileController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var viewNav: UIView!
    @IBOutlet weak var viewAva: UIView!
    @IBOutlet weak var imgAva: UIImageView!
    @IBOutlet weak var imgGantiAva: UIImageView!
    @IBOutlet weak var btnGantiAva: UIButton!
    @IBOutlet weak var txtKtp: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNamaDepan: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNamaBelakang: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTempatLahir: SkyFloatingLabelTextField!
    @IBOutlet weak var viewTglLahir: UIView!
    @IBOutlet weak var txtTglLahir: SkyFloatingLabelTextField!
    @IBOutlet weak var viewJenisKelamin: UIView!
    @IBOutlet weak var txtJenisKelamin: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTeleponRumah: SkyFloatingLabelTextField!
    @IBOutlet weak var viewProvinsi: UIView!
    @IBOutlet weak var txtProvinsi: SkyFloatingLabelTextField!
    @IBOutlet weak var viewKabKot: UIView!
    @IBOutlet weak var txtKabKot: SkyFloatingLabelTextField!
    @IBOutlet weak var viewKec: UIView!
    @IBOutlet weak var txtKec: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlamat: SkyFloatingLabelTextField!
    @IBOutlet weak var txtNamaKerabat: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAlamatKerabat: SkyFloatingLabelTextField!
    @IBOutlet weak var txtTelponKerabat: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    let imgPicker = UIImagePickerController()
    let dropdownJK = DropDown()
    let listProvinsi = DropDown()
    let listKabKot = DropDown()
    let listKecamatan = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.dropdownJK,
            self.listProvinsi,
            self.listKabKot,
            self.listKecamatan
        ] }()
    
    var idProvinsiSelected = 0
    var dataIdProvinsi = [Int]()
    var dataProvinsi = [String]()
    var idKabKotSelected = 0
    var dataIdKabKot = [Int]()
    var dataKabKot = [String]()
    var idKecamatanSelected = 0
    var dataIdKecamatan = [Int]()
    var dataKecamatan = [String]()
    
    var tglLahirSelected = ""
    var jkSelected = ""
    var fotoSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setDesign()
        self.showDataUser()
        self.setupTapGesture()
        self.getProvinsi()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.imgPicker.delegate = self
        //Nav
        self.viewNav.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNav.layer.shadowRadius = 1
        self.viewNav.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNav.layer.shadowOpacity = 0.5
        
        self.viewAva.layer.cornerRadius = self.viewAva.frame.width / 2
        self.imgAva.layer.cornerRadius = self.imgAva.frame.width / 2
        self.imgGantiAva.layer.cornerRadius = self.imgGantiAva.frame.width / 2
        self.btnSimpan.layer.cornerRadius = 5
    }
    
    func showDataUser(){
        if let data = try! Realm().objects(tb_user.self).first {
            if data.foto != "" {
                self.imgAva.loadImageUsingCacheWithUrlString(data.foto)
            }
            self.txtKtp.text = data.ktp
            self.txtNamaDepan.text = data.nama_depan
            self.txtNamaBelakang.text = data.nama_belakang
            self.txtTempatLahir.text = data.tempat_lahir
            if data.tanggal_lahir != "" {
                self.tglLahirSelected = data.tanggal_lahir
                let tglLahir = Helper.convertDate(data.tanggal_lahir, from: "yyyy-MM-dd", to: "dd MMMM yyyy")
                self.txtTglLahir.text = tglLahir
            }
            self.jkSelected = data.jk
            if data.jk == "L" {
                self.txtJenisKelamin.text = "Laki-laki"
            }else{
                self.txtJenisKelamin.text = "Perempuan"
            }
            self.txtTeleponRumah.text = data.telepon_rumah
            self.idProvinsiSelected = data.id_provinsi
//            self.lblProvinsi.text
            self.idKabKotSelected = data.id_kabkot
//            self.lblKabKot.text
            self.idKecamatanSelected = data.id_kecamatan
//            self.lblKec.text
            self.txtAlamat.text = data.alamat
            self.txtNamaKerabat.text = data.nama_kerabat
            self.txtAlamatKerabat.text = data.alamat_kerabat
            self.txtTelponKerabat.text = data.telepon_kerabat
        }
    }
    
    @IBAction func btnGantiAvaTapped(_ sender: UIButton) {
        let alert:UIAlertController=UIAlertController(title: "Ganti Foto Profil", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.modalPresentationStyle = .popover
        
        let cameraAction = UIAlertAction(title: "Kamera", style: UIAlertActionStyle.default) { UIAlertAction in
            self.cameraTapped()
        }
        let gallaryAction = UIAlertAction(title: "Galeri", style: UIAlertActionStyle.default) { UIAlertAction in
            self.libraryTapped()
        }
        let cancelAction = UIAlertAction(title: "Batal", style: UIAlertActionStyle.cancel) { UIAlertAction in }
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds;
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func cameraTapped(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {            
            self.imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imgPicker.allowsEditing = true
            self.present(self.imgPicker, animated: true, completion: nil)
        } else {
            self.libraryTapped()
        }
    }
    
    func libraryTapped(){
        self.imgPicker.allowsEditing = true
        self.imgPicker.sourceType = .photoLibrary
        //self.imgPicker.mediaTypes = [kUTTypeImage as String]
        present(imgPicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var pickedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        }
        self.imgAva.image = pickedImage
        //Save to local
        let imageData:NSData = UIImageJPEGRepresentation(pickedImage!, 0.5)! as NSData
        let imageExt = imageData.imageFormat
        let imageUrl = Helper.getDocumentsDirectory().appendingPathComponent("profil.\(imageExt)")
        try? imageData.write(to: URL(fileURLWithPath: imageUrl), options: [.atomic])
        //Encode to Base64
        let imageNsData = NSData(contentsOf: URL(fileURLWithPath: imageUrl))
        let imgBase64 = imageNsData!.base64EncodedString(options: .init(rawValue: 0))
        self.fotoSelected = "data:image/\(imageExt);base64,\(imgBase64)"
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func setupTapGesture(){
        let tglLahirTap = UITapGestureRecognizer(target: self, action: #selector(self.tglLahirTapped(_:)))
        self.viewTglLahir.isUserInteractionEnabled = true
        self.viewTglLahir.addGestureRecognizer(tglLahirTap)
        self.setupDropdownJK()
        let jkTap = UITapGestureRecognizer(target: self, action: #selector(self.jkTapped(_:)))
        self.viewJenisKelamin.isUserInteractionEnabled = true
        self.viewJenisKelamin.addGestureRecognizer(jkTap)
        let provinsiTap = UITapGestureRecognizer(target: self, action: #selector(self.provinsiTapped(_:)))
        self.viewProvinsi.isUserInteractionEnabled = true
        self.viewProvinsi.addGestureRecognizer(provinsiTap)
        let kabkotTap = UITapGestureRecognizer(target: self, action: #selector(self.kabkotTapped(_:)))
        self.viewKabKot.isUserInteractionEnabled = true
        self.viewKabKot.addGestureRecognizer(kabkotTap)
        let kecamatanTap = UITapGestureRecognizer(target: self, action: #selector(self.kecamatanTapped(_:)))
        self.viewKec.isUserInteractionEnabled = true
        self.viewKec.addGestureRecognizer(kecamatanTap)
    }
    
    func tglLahirTapped(_ sender: UITapGestureRecognizer){
        DatePickerDialog().show("Tanggal Lahir", doneButtonTitle: "Pilih", cancelButtonTitle: "Batal", maximumDate: Date() , datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                self.tglLahirSelected = formatter.string(from: dt)
                formatter.dateFormat = "dd MMMM YYYY"
                self.txtTglLahir.text = formatter.string(from: dt)
            }
        }
    }
    
    func jkTapped(_ sender: UITapGestureRecognizer){
        self.dropdownJK.show()
    }
    
    func provinsiTapped(_ sender: UITapGestureRecognizer){
        self.listProvinsi.show()
    }
    
    func kabkotTapped(_ sender: UITapGestureRecognizer){
        self.listKabKot.show()
    }
    
    func kecamatanTapped(_ sender: UITapGestureRecognizer){
        self.listKecamatan.show()
    }
    
    func setupDropdownJK(){
        self.dropdownJK.anchorView = self.txtJenisKelamin
        self.dropdownJK.bottomOffset = CGPoint(x: 0, y: self.txtJenisKelamin.bounds.height)
        self.dropdownJK.direction = .bottom
        self.dropdownJK.dataSource = ["Laki - Laki", "Perempuan"]
        
        self.dropdownJK.selectionAction = { [unowned self] (index, item) in
            self.txtJenisKelamin.text = item
            self.txtJenisKelamin.textColor = UIColor.black
            if item == "Laki - Laki" {
                self.jkSelected = "L"
            } else {
                self.jkSelected = "P"
            }
        }
    }
    
    func setupListProvinsi(){
        self.listProvinsi.anchorView = self.txtProvinsi
        self.listProvinsi.bottomOffset = CGPoint(x: 0, y: self.txtProvinsi.bounds.height)
        self.listProvinsi.direction = .bottom
        self.listProvinsi.dataSource = self.dataProvinsi
        self.listProvinsi.selectionAction = { [unowned self] (index, item) in
            self.txtProvinsi.text = item
            self.txtProvinsi.textColor = UIColor.black
            let data = self.dataProvinsi.index(of: item)
            let id = self.dataIdProvinsi[data!]
            self.idProvinsiSelected = id
            self.getKabKotByProvinsi()
        }
    }
    
    func setupListKabKot(){
        self.listKabKot.anchorView = self.txtKabKot
        self.listKabKot.bottomOffset = CGPoint(x: 0, y: self.txtKabKot.bounds.height)
        self.listKabKot.direction = .bottom
        self.listKabKot.dataSource = self.dataKabKot
        self.listKabKot.selectionAction = { [unowned self] (index, item) in
            self.txtKabKot.text = item
            self.txtKabKot.textColor = UIColor.black
            let data = self.dataKabKot.index(of: item)
            let id = self.dataIdKabKot[data!]
            self.idKabKotSelected = id
            self.getkecamtanByKabKot()
        }
    }
    
    func setupListKecamatan(){
        self.listKecamatan.anchorView = self.txtKec
        self.listKecamatan.bottomOffset = CGPoint(x: 0, y: self.txtKec.bounds.height)
        self.listKecamatan.direction = .bottom
        self.listKecamatan.dataSource = self.dataKecamatan
        self.listKecamatan.selectionAction = { [unowned self] (index, item) in
            self.txtKec.text = item
            self.txtKec.textColor = UIColor.black
            let data = self.dataKecamatan.index(of: item)
            let id = self.dataIdKecamatan[data!]
            self.idKecamatanSelected = id
        }
    }
    
    func getProvinsi(){
        let url = "\(Helper.baseUrl)v1/provinsi"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataProvinsi.removeAll()
                    self.dataIdProvinsi.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["province"].stringValue
                        self.dataProvinsi.append(data)
                        let id = jason[i]["province_id"].intValue
                        self.dataIdProvinsi.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListProvinsi()
        }
    }
    
    func getKabKotByProvinsi(){
        let url = "\(Helper.baseUrl)v1/kabkot/provinsi/\(self.idProvinsiSelected)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataKabKot.removeAll()
                    self.dataIdKabKot.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["city_name"].stringValue
                        let type = jason[i]["type"].stringValue
                        let kabkot = type + " " + data
                        self.dataKabKot.append(kabkot)
                        let id = jason[i]["city_id"].intValue
                        self.dataIdKabKot.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListKabKot()
        }
    }
    
    func getkecamtanByKabKot(){
        let url = "\(Helper.baseUrl)v1/kecamatan/kabkot/\(self.idKabKotSelected)"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    self.dataKecamatan.removeAll()
                    self.dataIdKecamatan.removeAll()
                    for i in 0..<jason.count{
                        let data = jason[i]["subdistrict_name"].stringValue
                        self.dataKecamatan.append(data)
                        let id = jason[i]["subdistrict_id"].intValue
                        self.dataIdKecamatan.append(id)
                    }
                }else{
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }.responseData { (Success) in
                self.setupListKecamatan()
        }
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
        self.simpanProfil()
    }
    
    func simpanProfil(){
        var url = ""
        if let userData = try! Realm().objects(tb_user.self).first {
            url = "\(Helper.baseUrl)v1/user_details/\(userData.id)"
        }
        let dataToken = try! Realm().objects(tb_token.self).first
        let headers = [
            "Authorization" : "Bearer \(dataToken!.access_token)",
            "Content-Type" : "application/json",
            "Accept" : "application/json"
        ]
        let body:[String : Any] = [
            "nama_depan" : self.txtNamaDepan.text!,
            "nama_belakang" : self.txtNamaBelakang.text!,
            "tempat_lahir" : self.txtTempatLahir.text!,
            "tanggal_lahir" : self.tglLahirSelected,
            "jk" : self.jkSelected,
            "ktp" : self.txtKtp.text!,
            "telepon_rumah" : self.txtTeleponRumah.text!,
            "id_provinsi" : self.idProvinsiSelected,
            "id_kabkot" : self.idKabKotSelected,
            "id_kecamatan" : self.idKecamatanSelected,
            "kelurahan" : "",
            "alamat" : self.txtAlamat.text!,
            "nama_kerabat" : self.txtNamaKerabat.text!,
            "alamat_kerabat" : self.txtAlamatKerabat.text!,
            "telepon_kerabat" : self.txtTelponKerabat.text!,
            "foto" : self.fotoSelected
        ]
        Alamofire.request(url, method: .put, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let user = jason["user"]
                    let detail = jason["detail"]
                    let message = jason["message"].stringValue
                    print(jason)
                    
                    if message == "OK" {
                        if let modelUser = try! Realm().objects(tb_user.self).first {
                            try! Realm().write({ 
                                modelUser.kode_user = user["kode_user"].stringValue
                                modelUser.email = user["email"].stringValue
                                modelUser.telepon = user["telepon"].stringValue
                                modelUser.nama_depan = user["nama_depan"].stringValue
                                modelUser.nama_belakang = user["nama_belakang"].stringValue
                                modelUser.jk = user["jk"].stringValue
                                modelUser.ktp = user["ktp"].stringValue
                                modelUser.role_id = user["role_id"].intValue
                                modelUser.role = user["role"].stringValue
                                modelUser.nama_role = user["nama_role"].stringValue
                                modelUser.saldo = user["saldo"].intValue
                                modelUser.fee = user["fee"].intValue
                                modelUser.poin = user["poin"].intValue
                                modelUser.seat = user["seat"].intValue
                                modelUser.id = detail["id"].intValue
                                modelUser.kode_paket = detail["kode_paket"].stringValue
                                modelUser.kode_kantor = detail["kode_kantor"].stringValue
                                modelUser.tempat_lahir = detail["tempat_lahir"].stringValue
                                modelUser.tanggal_lahir = detail["tanggal_lahir"].stringValue
                                modelUser.nama_kerabat = detail["nama_kerabat"].stringValue
                                modelUser.alamat_kerabat = detail["alamat_kerabat"].stringValue
                                modelUser.telepon_kerabat = detail["telepon_kerabat"].stringValue
                                modelUser.telepon_rumah = detail["telepon_rumah"].stringValue
                                modelUser.id_provinsi = detail["id_provinsi"].intValue
                                modelUser.id_kabkot = detail["id_kabkot"].intValue
                                modelUser.id_kecamatan = detail["id_kecamatan"].intValue
                                modelUser.kelurahan = detail["kelurahan"].stringValue
                                modelUser.alamat = detail["alamat"].stringValue
                                modelUser.foto = detail["foto"]["url"].stringValue
                                modelUser.latitude = detail["latitude"].intValue
                                modelUser.longitude = detail["longitude"].intValue
                            })
                        }
                    }
                    if message == "OK" {
                        let alert = UIAlertController(title: "Info", message: "Edit Profil Berhasil", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
