//
//  ProfileController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/12/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Alamofire
import SwiftyJSON

class ProfileController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var viewEditProfile: UIView!
    @IBOutlet weak var viewImgProfile: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblNama: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblIdUser: UILabel!
    @IBOutlet weak var lblDeposit: UILabel!
    @IBOutlet weak var btnDetailDeposit: UIButton!
    @IBOutlet weak var viewCari: UIView!
    @IBOutlet weak var constViewCari: NSLayoutConstraint!
    @IBOutlet weak var progress1: UIProgressView!
    @IBOutlet weak var progress2: UIProgressView!
    @IBOutlet weak var progress3: UIProgressView!
    var progress: [UIProgressView] { return [ self.progress1, self.progress2, self.progress3 ] }
    @IBOutlet weak var icoArrowProgress: UIImageView!
    @IBOutlet weak var icoCheckEmail: UIImageView!
    @IBOutlet weak var lblCheckEmail: UILabel!
    @IBOutlet weak var icoCheckAlamat: UIImageView!
    @IBOutlet weak var lblCheckAlamat: UILabel!
    @IBOutlet weak var icoCheckNoTelpon: UIImageView!
    @IBOutlet weak var lblCheckNoTelpon: UILabel!
    @IBOutlet weak var colMenuProfile: UICollectionView!
    @IBOutlet weak var viewPengaturanAkun: UIView!
    @IBOutlet weak var constColMenuProfile: NSLayoutConstraint!
    @IBOutlet weak var viewPrivasi: UIView!
    @IBOutlet weak var viewSyarat: UIView!
    @IBOutlet weak var viewTentang: UIView!
    @IBOutlet weak var viewUpgrade: UIView!
    @IBOutlet weak var constUpgrade: NSLayoutConstraint!
    @IBOutlet weak var btnUpgrade: UIButton!
    
    var dataMenu = [String]()
    var dataImgMenu = [String]()
    var isDetailVerify = false
    var totalDetailVerify = 0
    
    let sizeCell:CGFloat = (UIScreen.main.bounds.size.width - 88) / 4
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        self.setDesign()
        
        let menu = ["Poin", "Voucher", "Transaksi", "Riwayat"]
        self.dataMenu = menu
        let imgMenu = ["poin", "voucher", "transaksi", "setting"]
        self.dataImgMenu = imgMenu
        self.colMenuProfile.delegate = self
        self.colMenuProfile.dataSource = self
        
        let editDataProfileTap = UITapGestureRecognizer(target: self, action: #selector(self.editDataProfileTapped(_:)))
        self.viewEditProfile.isUserInteractionEnabled = true
        self.viewEditProfile.addGestureRecognizer(editDataProfileTap)
        
        let editProfileTap = UITapGestureRecognizer(target: self, action: #selector(self.editProfileTapped(_:)))
        self.viewPengaturanAkun.isUserInteractionEnabled = true
        self.viewPengaturanAkun.addGestureRecognizer(editProfileTap)
        
        let checkEmailTap = UITapGestureRecognizer(target: self, action: #selector(self.checkEmailTapped(_:)))
        self.lblCheckEmail.addGestureRecognizer(checkEmailTap)
        let checkAlamatTap = UITapGestureRecognizer(target: self, action: #selector(self.checkAlamatTapped(_:)))
        self.lblCheckAlamat.addGestureRecognizer(checkAlamatTap)
        let checkTelponTap = UITapGestureRecognizer(target: self, action: #selector(self.checkTelponTapped(_:)))
        self.lblCheckNoTelpon.addGestureRecognizer(checkTelponTap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
        if let dataUser = try! Realm().objects(tb_user.self).first {
            if dataUser.id == 0 {
                self.getUserDetail()
            } else {
                self.setDetailUser()
            }
        }else{
            self.getUserDetail()
        }
        self.getProgressUser()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.viewImgProfile.layer.cornerRadius = self.viewImgProfile.frame.height / 2
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height / 2
        self.btnDetailDeposit.layer.cornerRadius = 5
        self.progress1.layer.cornerRadius = 4
        self.progress1.layer.borderColor = Helper.customColorGreen2.cgColor
        self.progress1.layer.borderWidth = 1
        self.progress2.layer.cornerRadius = 4
        self.progress2.layer.borderColor = Helper.customColorGreen2.cgColor
        self.progress2.layer.borderWidth = 1
        self.progress3.layer.cornerRadius = 4
        self.progress3.layer.borderColor = Helper.customColorGreen2.cgColor
        self.progress3.layer.borderWidth = 1
        self.btnUpgrade.layer.cornerRadius = 5
        self.btnUpgrade.layer.borderColor = Helper.customColorGreen2.cgColor
        self.btnUpgrade.layer.borderWidth = 1
        self.constColMenuProfile.constant = self.sizeCell
    }
    
    func getUserDetail(){
        let url = "\(Helper.baseUrl)v1/user_details"
        var dataToken = tb_token()
        if let token = try! Realm().objects(tb_token.self).first {
            dataToken = token
        }
        let headers = [
            "x-per-page" : "10",
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let user = jason["user"]
                    let detail = jason["detail"]
                    print(jason)
                    
                    if jason["message"].stringValue == "OK" {
                        if let modelUser = try! Realm().objects(tb_user.self).first {
                            try! Realm().write({ 
                                modelUser.kode_user = user["kode_user"].stringValue
                                modelUser.email = user["email"].stringValue
                                modelUser.telepon = user["telepon"].stringValue
                                modelUser.nama_depan = user["nama_depan"].stringValue
                                modelUser.nama_belakang = user["nama_belakang"].stringValue
                                modelUser.jk = user["jk"].stringValue
                                modelUser.ktp = user["ktp"].stringValue
                                modelUser.role_id = user["role_id"].intValue
                                modelUser.role = user["role"].stringValue
                                modelUser.nama_role = user["nama_role"].stringValue
                                modelUser.saldo = user["saldo"].intValue
                                modelUser.fee = user["fee"].intValue
                                modelUser.poin = user["poin"].intValue
                                modelUser.seat = user["seat"].intValue
                                modelUser.id = detail["id"].intValue
                                modelUser.kode_paket = detail["kode_paket"].stringValue
                                modelUser.kode_kantor = detail["kode_kantor"].stringValue
                                modelUser.tempat_lahir = detail["tempat_lahir"].stringValue
                                modelUser.tanggal_lahir = detail["tanggal_lahir"].stringValue
                                modelUser.nama_kerabat = detail["nama_kerabat"].stringValue
                                modelUser.alamat_kerabat = detail["alamat_kerabat"].stringValue
                                modelUser.telepon_kerabat = detail["telepon_kerabat"].stringValue
                                modelUser.telepon_rumah = detail["telepon_rumah"].stringValue
                                modelUser.id_provinsi = detail["id_provinsi"].intValue
                                modelUser.id_kabkot = detail["id_kabkot"].intValue
                                modelUser.id_kecamatan = detail["id_kecamatan"].intValue
                                modelUser.kelurahan = detail["kelurahan"].stringValue
                                modelUser.alamat = detail["alamat"].stringValue
                                modelUser.foto = detail["foto"]["url"].stringValue
                                modelUser.latitude = detail["latitude"].intValue
                                modelUser.longitude = detail["longitude"].intValue
                            })
                        }
                    }
                }else{
                    print("request profile user gagal")
                }
            }.responseData { (Success) in
                self.setDetailUser()
        }
    }
    
    func setDetailUser(){
        if let data = try! Realm().objects(tb_user.self).first {
            if data.foto != "" {
                self.imgProfile.loadImageUsingCacheWithUrlString(data.foto)
            }
            self.lblNama.text = data.nama_depan + " " + data.nama_belakang
            self.lblStatus.text = data.nama_role
            self.lblIdUser.text = data.kode_user
        }
    }
    
    func getProgressUser(){
        let url = "\(Helper.baseUrl)v1/users/progress"
        var dataToken = tb_token()
        if let token = try! Realm().objects(tb_token.self).first {
            dataToken = token
        }
        let headers = [
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let data = JSON(jason)["data"]
                    self.totalDetailVerify = 0
                    if data["email"].intValue == 1 {
                        self.totalDetailVerify += 1
                        self.icoCheckEmail.image = UIImage(named: "ic-checked-default")
                        self.lblCheckEmail.textColor = Helper.customColorGreen2
                    }else{
                        self.icoCheckEmail.image = UIImage(named: "ico_warning")
                        self.lblCheckEmail.textColor = UIColor.init(hex: "D10400")
                        self.lblCheckEmail.isUserInteractionEnabled = true
                    }
                    if data["alamat"].intValue == 1 {
                        self.totalDetailVerify += 1
                        self.icoCheckAlamat.image = UIImage(named: "ic-checked-default")
                        self.lblCheckAlamat.textColor = Helper.customColorGreen2
                    }else{
                        self.icoCheckAlamat.image = UIImage(named: "ico_warning")
                        self.lblCheckAlamat.textColor = UIColor.init(hex: "D10400")
                        self.lblCheckAlamat.isUserInteractionEnabled = true
                    }
                    if data["handphone"].intValue == 1 {
                        self.totalDetailVerify += 1
                        self.icoCheckNoTelpon.image = UIImage(named: "ic-checked-default")
                        self.lblCheckNoTelpon.textColor = Helper.customColorGreen2
                    }else{
                        self.icoCheckNoTelpon.image = UIImage(named: "ico_warning")
                        self.lblCheckNoTelpon.textColor = UIColor.init(hex: "D10400")
                        self.lblCheckNoTelpon.isUserInteractionEnabled = true
                    }
                    for i in 0..<self.totalDetailVerify {
                        self.progress[i].progress = 1
                    }
                }else{
                    print("request progress gagal")
                }
        }
    }
    
    func editDataProfileTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showEditDataProfile", sender: self)
    }
    
    @IBAction func btnDetailDepositTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showDeposit", sender: self)
    }
    
    @IBAction func btnDetailVerifyTapped(_ sender: UIButton) {
        if self.isDetailVerify {
            UIView.animate(withDuration: 0.5, animations: {
                self.constViewCari.constant = 32
                self.icoArrowProgress.transform = CGAffineTransform(rotationAngle: 0.0)
                self.view.layoutIfNeeded()
            })
            self.isDetailVerify = false
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.constViewCari.constant = 106
                self.icoArrowProgress.transform = CGAffineTransform(rotationAngle: 15.75)
                self.view.layoutIfNeeded()
            })
            self.isDetailVerify = true
        }
    }
    
    func checkEmailTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showEmail", sender: self)
    }
    func checkAlamatTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showEditDataProfile", sender: self)
    }
    func checkTelponTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showNoTelpon", sender: self)
    }
    
    func editProfileTapped(_ sender: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "showEditProfile", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dataCell", for: indexPath) as! ProfileCell
        
        cell.viewContainer.layer.borderColor = UIColor.white.cgColor
        cell.lblMenu.text = self.dataMenu[indexPath.row]
        cell.imgMenu.image = UIImage(named: self.dataImgMenu[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: sizeCell, height: sizeCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    @IBAction func btnUpgradeTapped(_ sender: UIButton) {
        
    }
}
