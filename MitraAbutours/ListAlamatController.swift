//
//  ListAlamatController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/27/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class ListAlamatController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var tbData: UITableView!
    
    var isEdit = 0
    var jsonEdit: JSON!
    var data: JSON!
    var kodeAlamatSelected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.getListAlamat()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
    }
    
    func getListAlamat(){
        let url = "\(Helper.baseUrl)v1/alamat"
        let dataToken = try! Realm().objects(tb_token.self).first!
        let headers = [
            "x-per-page" : "10",
            "x-page" : "1",
            "Authorization" : "Bearer \(dataToken.access_token)"
        ]
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let data = jason["data"]
                    print(jason)
                    self.data = data
                    
                    self.tbData.delegate = self
                    self.tbData.dataSource = self
                    self.tbData.reloadData()
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! ListAlamatCell
        
        let data = self.data[indexPath.row]
        cell.lblNama.text = data["nama_alamat"].stringValue
        let alamat = data["alamat"].stringValue //+ "\n" +
        cell.lblAlamat.text = alamat
        cell.imgDefaultAlamat.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        let data = self.data[indexPath.row]
        let width = UIScreen.main.bounds.size.width - 32
        height = Helper.estimateFrameForText(data["alamat"].stringValue, width: Int(width)).height + 64
        print(height)
        return height
    }
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
        if let cell = sender.superview?.superview?.superview as? ListAlamatCell {
            let indexPath = tbData.indexPath(for: cell)
            self.jsonEdit = self.data[indexPath!.row]
            self.kodeAlamatSelected = self.data[indexPath!.row]["id"].stringValue
        }
        let popController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popoverActionList") as! PopoverListController
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.preferredContentSize = CGSize(width: 120, height: 80)
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        popController.dari = 1
        self.present(popController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func deleteDataAlamat(){
        let alert = UIAlertController(title: "Info", message: "Hapus Data Alamat?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YA", style: .default, handler: { action in
            let url = "\(Helper.baseUrl)v1/alamat/\(self.kodeAlamatSelected)"
            let dataToken = try! Realm().objects(tb_token.self).first!
            let headers = [
                "Authorization" : "Bearer \(dataToken.access_token)",
                "Content-Type" : "application/json"
            ]
            Alamofire.request(url, method: .delete, encoding: JSONEncoding.default, headers: headers)
                .responseJSON{response in
                    if let jason = response.result.value {
                        let jason = JSON(jason)
                        print(jason)
                        self.getListAlamat()
                    }else{
                        print("request gagal")
                        let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                        self.present(alert, animated: true, completion: nil)
                    }
            }
        }))
        alert.addAction(UIAlertAction(title: "BATAL", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnTambahTapped(_ sender: UIButton) {
        self.isEdit = 0
        self.performSegue(withIdentifier: "showActionAlamat", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showActionAlamat" {
            let conn = segue.destination as! AlamatController
            conn.isEdit = self.isEdit
            conn.jsonEdit = self.jsonEdit
        }
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
