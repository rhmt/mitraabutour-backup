//
//  RegistrasiBerhasilController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/2/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class RegisterBerhasilController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSelesaiTapped(_ sender: UIButton) {
        print("pindah")
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showHome") as! TabbarHomeController
        self.present(Popover, animated: true, completion: nil)
    }
}
