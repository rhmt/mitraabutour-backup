//
//  GantiTelponController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/28/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import RealmSwift

class GantiTelponController: UIViewController {
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var lblInfoNoTelpon: UILabel!
    @IBOutlet weak var txtNoTelponBaru: SkyFloatingLabelTextField!
    @IBOutlet weak var txtKataSandi: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSimpan: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.setDesign()
        let dataUser = try! Realm().objects(tb_user.self).first!
        self.lblInfoNoTelpon.text = dataUser.telepon
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        //Nav
        self.viewNavBar.layer.shadowColor = Helper.customColorCoklat.cgColor
        self.viewNavBar.layer.shadowRadius = 1
        self.viewNavBar.layer.shadowOffset = CGSize(width: 0, height: 0.25)
        self.viewNavBar.layer.shadowOpacity = 0.5
        //View Info
        self.viewInfo.layer.borderWidth = 1
        self.viewInfo.layer.borderColor = UIColor.lightGray.cgColor
        self.viewInfo.layer.cornerRadius = 5
        //BtnSimpan
        self.btnSimpan.layer.cornerRadius = 5
    }
    
    @IBAction func btnSimpanTapped(_ sender: UIButton) {
    
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
