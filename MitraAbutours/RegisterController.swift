//
//  RegisterController.swift
//  MitraAbutours
//
//  Created by Caryta on 8/31/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class RegisterController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tbData: UITableView!
    @IBOutlet weak var constHeightTbData: NSLayoutConstraint!
    
    var titleData = [String]()
    var subTitleData = [String]()
    let widthLblSubTitle = UIScreen.main.bounds.size.width - 116
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Testing
        self.titleData.append("Plus Gold")
        self.subTitleData.append("Diskon Umrah")
        self.titleData.append("Plus Gold 2")
        self.subTitleData.append("Diskon Umrah . Diskon Umrah . Diskon Umrah")
        self.titleData.append("Plus Gold 3")
        self.subTitleData.append("Diskon Umrah . Diskon Umrah . Diskon Umrah . Diskon Umrah . Diskon Umrah . Diskon Umrah . Diskon Umrah")
        
        self.tbData.delegate = self
        self.tbData.dataSource = self
        
        self.setHeightTbData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setHeightTbData(){
        var heightTbData:CGFloat = 0
        for strSubTitle in self.subTitleData {
            var height:CGFloat = 0
            let heightLblSubTitle = Helper.estimateFrameForText(strSubTitle, width: Int(self.widthLblSubTitle)).height
            height = heightLblSubTitle + 71
            if height > 106 {
                height = 106
            }
            heightTbData += height
        }
        self.constHeightTbData.constant = heightTbData + 16
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! RegisterCell
        
        //Atur Tinggi SubTitle
        var heightLblSubTitle = Helper.estimateFrameForText(self.subTitleData[indexPath.row], width: Int(cell.lblSubTitle.frame.width)).height
        if heightLblSubTitle > 40 {
            heightLblSubTitle = 40
        }
        cell.constLblSubTitle.constant = heightLblSubTitle + 4
        
        cell.lblTitle.text = self.titleData[indexPath.row]
        cell.lblSubTitle.text = self.subTitleData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let Popover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "popupSnK") as! SyaratKetentuanController
        self.addChildViewController(Popover)
        Popover.view.frame = self.view.frame
        self.view.addSubview(Popover.view)
        Popover.didMove(toParentViewController: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 106
        let heightLblSubTitle = Helper.estimateFrameForText(self.subTitleData[indexPath.row], width: Int(self.widthLblSubTitle)).height
        height = heightLblSubTitle + 71
        if height > 106 {
            height = 106
        }
        return height
    }
}
