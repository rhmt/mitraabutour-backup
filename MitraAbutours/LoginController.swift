//
//  LoginController.swift
//  MitraAbutours
//
//  Created by Caryta on 8/30/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class LoginController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var imgSee: UIImageView!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var seePasswordStatus: Bool = false
    var alertLoading: UIAlertController!
    var titleMsgError = ""
    var msgError = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapSee = UITapGestureRecognizer(target: self, action: #selector(self.seePassword(_:)))
        tapSee.numberOfTapsRequired = 1
        self.imgSee.isUserInteractionEnabled = true
        self.imgSee.addGestureRecognizer(tapSee)
        
        self.txtUsername.delegate = self
        self.txtPassword.delegate = self
        
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.btnLogin.layer.cornerRadius = 5
        self.btnRegister.layer.cornerRadius = 5
        self.btnRegister.layer.borderColor = Helper.customColorGreen1.cgColor
        self.btnRegister.layer.borderWidth = 1
    }
    
    func seePassword(_ sender: UITapGestureRecognizer){
        if self.seePasswordStatus {
            self.txtPassword.isSecureTextEntry = true
            self.seePasswordStatus = false
        }else{
            self.txtPassword.isSecureTextEntry = false
            self.seePasswordStatus = true
        }
    }
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        //Validasi kelengkapan data
        if self.txtUsername.text == "" || self.txtPassword.text == "" {
            let alert = UIAlertController(title: "Info", message: "Silahkan isi No Telpon atau Email dan Kata Sandi", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        //Validasi Ada Koneksi atau tidak
        if Helper.isInternetAvailable() {
            //Login
            self.alertLoading = UIAlertController(title: "Sedang Masuk", message: "Silahkan tunggu ...", preferredStyle: UIAlertControllerStyle.alert)
            self.present(self.alertLoading, animated: true, completion: nil)
            self.login()
        }else{
            //Ga ada Koneksi
            let alert = UIAlertController(title: "Tidak ada jaringan internet", message: "Silahkan sambungkan ke internet terlebih dahulu", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }

    }
    
    func login(){
        let url = "\(Helper.baseUrl)v1/users/login"
        let headers = [
            "x-username" : self.txtUsername.text!,
            "x-password" : self.txtPassword.text!,
            "x-client-id" : Helper.clientId,
            "x-device-id" : Helper.uid,
            "x-device-model" : Helper.deviceName,
            "x-app-version" : Helper.appVersion,
            //"x-otp-via" : "json" //Testing
            ]
        print(headers)
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    let success = jason["success"].stringValue
                    if success == "false" {
                        let otp = jason["otp"].stringValue
                        if otp == "true" {
                            self.alertLoading.dismiss(animated: true, completion: self.showOtp)
                        }else{
                            self.titleMsgError = "Gagal"
                            self.msgError = jason["message"].stringValue
                            self.alertLoading.dismiss(animated: true, completion: self.showErrorMsg)
                        }
                    }else{
                        let data = jason["data"]
                        let modelToken = tb_token()
                        modelToken.access_token = jason["access_token"].stringValue
                        modelToken.token_type = jason["token_type"].stringValue
                        modelToken.expires_in = jason["expires_in"].intValue
                        modelToken.refresh_token = jason["access_token"].stringValue
                        DBHelper.insert(modelToken)
                        let modelUser = tb_user()
                        modelUser.kode_user = data["kode_user"].stringValue
                        modelUser.email = data["email"].stringValue
                        modelUser.telepon = data["telepon"].stringValue
                        modelUser.nama_depan = data["nama_depan"].stringValue
                        modelUser.nama_belakang = data["nama_belakang"].stringValue
                        modelUser.jk = data["jk"].stringValue
                        modelUser.ktp = data["ktp"].stringValue
                        modelUser.role_id = data["role_id"].intValue
                        modelUser.role = data["role"].stringValue
                        modelUser.nama_role = data["nama_role"].stringValue
                        modelUser.saldo = data["saldo"].intValue
                        modelUser.fee = data["fee"].intValue
                        modelUser.poin = data["poin"].intValue
                        modelUser.seat = data["seat"].intValue
                        modelUser.kode_referal = data["kode_referal"].stringValue
                        DBHelper.insert(modelUser)
                        self.alertLoading.dismiss(animated: true, completion: self.loginBerhasil)
                    }
                }else{
                    print("request gagal")
                    self.titleMsgError = "Error"
                    self.msgError = "Terjadi kesalahan, silahkan coba beberapa saat lagi"
                    self.alertLoading.dismiss(animated: true, completion: self.showErrorMsg)
                }
        }
    }
    
    func loginBerhasil(){
        let show = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showHome") as! TabbarHomeController
        self.present(show, animated: true, completion: nil)
    }
    
    func showOtp(){
        let show = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showOtp") as! KonfirmasiOTPController
        show.username = self.txtUsername.text!
        show.from = 2
        self.present(show, animated: true, completion: nil)
    }
    
    func showErrorMsg(){
        let alert = UIAlertController(title: self.titleMsgError, message: self.msgError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
}
