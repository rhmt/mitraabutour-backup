//
//  SyaratKetentuanController.swift
//  MitraAbutours
//
//  Created by Caryta on 8/31/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class SyaratKetentuanController: UIViewController {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgClose: UIImageView!
    @IBOutlet weak var skSegmented: UISegmentedControl!
    @IBOutlet weak var tvData: UITextView!
    @IBOutlet weak var imgAgree: UIImageView!
    @IBOutlet weak var lblAgree: UILabel!
    
    var dataSK = [String]()
    var statusAgree: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showAnimate()
        self.setTapGesture()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.viewContainer.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Testing
        self.dataSK.append("data Syarat dan Ketentuan")
        self.dataSK.append("data Fasilitas")
        self.selectedSkSegment()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setTapGesture(){
        //Click to close
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(self.closePopup(_:)))
        tapBack.numberOfTapsRequired = 1
        self.viewContainer.isUserInteractionEnabled = true
        self.viewContainer.addGestureRecognizer(tapBack)
        //Click to Check
        let tapCheck = UITapGestureRecognizer(target: self, action: #selector(self.checkLblTapped(_:)))
        tapCheck.numberOfTapsRequired = 1
        self.lblAgree.isUserInteractionEnabled = true
        self.lblAgree.addGestureRecognizer(tapCheck)
        let tapCheck2 = UITapGestureRecognizer(target: self, action: #selector(self.checkTapped(_:)))
        tapCheck2.numberOfTapsRequired = 1
        self.imgAgree.isUserInteractionEnabled = true
        self.imgAgree.addGestureRecognizer(tapCheck2)
    }
    
    @IBAction func skSegmented(_ sender: UISegmentedControl) {
        self.selectedSkSegment()
    }
    
    func selectedSkSegment(){
        let indexSegment = self.skSegmented.selectedSegmentIndex
        self.tvData.text = self.dataSK[indexSegment]
    }
    
    func checkTapped(_ sender: UITapGestureRecognizer){
        self.agreeTapped()
    }
    func checkLblTapped(_ sender: UITapGestureRecognizer){
        self.agreeTapped()
    }
    
    func agreeTapped(){
        if self.statusAgree {
            self.statusAgree = false
            self.imgAgree.image = UIImage(named: "ico_unchecked")
        }else{
            self.statusAgree = true
            self.imgAgree.image = UIImage(named: "ico_checked")
        }
    }
    
    @IBAction func btnLanjutTapped(_ sender: UIButton) {
        if self.statusAgree {
            self.performSegue(withIdentifier: "showFormulir", sender: self)
        }else{
            let alert = UIAlertController(title: "Info", message: "Silahkan setujui dahulu", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnClose(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    @IBAction func btnKembali(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    func closePopup(_ sender: UITapGestureRecognizer){
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }, completion: {(finished: Bool) in
            if(finished){
                self.view.removeFromSuperview()
            }
        })
    }

}
