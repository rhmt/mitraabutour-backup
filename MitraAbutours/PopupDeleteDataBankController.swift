//
//  PopupDeleteDataBankController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/18/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit

class PopupDeleteDataBankController: UIViewController {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnBatal: UIButton!
    @IBOutlet weak var btnYa: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDesign()
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        Helper.tokenInfo(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.btnBatal.layer.cornerRadius = self.btnBatal.frame.height / 2
        self.btnYa.layer.cornerRadius = self.btnYa.frame.height / 2
        self.btnYa.layer.borderColor = UIColor.white.cgColor
        self.btnYa.layer.borderWidth = 1
    }
    
    @IBAction func btnBatalTapped(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    @IBAction func btnYaTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0.0
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        }, completion: {(finished: Bool) in
            if(finished){
                self.view.removeFromSuperview()
            }
        })
    }
}
