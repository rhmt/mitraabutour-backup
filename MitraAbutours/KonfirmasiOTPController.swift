//
//  KonfirmasiOTPController.swift
//  MitraAbutours
//
//  Created by Caryta on 9/2/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class KonfirmasiOTPController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblNoTelp: UILabel!
    @IBOutlet weak var txtKode1: UITextField!
    @IBOutlet weak var txtKode2: UITextField!
    @IBOutlet weak var txtKode3: UITextField!
    @IBOutlet weak var txtKode4: UITextField!
    @IBOutlet weak var txtKode5: UITextField!
    @IBOutlet weak var txtKode6: UITextField!
    @IBOutlet weak var btnKirim: UIButton!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    var from = 0 //1:Register, 2:Login
    var username = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblNoTelp.text = self.username
        self.btnKirim.layer.cornerRadius = 5
        self.txtKode1.delegate = self
        self.txtKode2.delegate = self
        self.txtKode3.delegate = self
        self.txtKode4.delegate = self
        self.txtKode5.delegate = self
        self.txtKode6.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = textField.text!.characters.count + string.characters.count - range.length
        return length <= 1
    }
    
    @IBAction func txtKodeChanged(_ sender: UITextField) {
        self.textChange(sender)
    }
    
    func textChange(_ sender: UITextField){
        switch sender {
        case self.txtKode1:
            if self.txtKode1.text!.characters.count > 0 {
                self.txtKode1.resignFirstResponder()
                self.txtKode2.becomeFirstResponder()
            }
        case self.txtKode2:
            if self.txtKode2.text!.characters.count > 0 {
                self.txtKode3.becomeFirstResponder()
            }
            if self.txtKode2.text!.characters.count == 0 {
                self.txtKode1.becomeFirstResponder()
            }
            self.txtKode2.resignFirstResponder()
        case self.txtKode3:
            if self.txtKode3.text!.characters.count > 0 {
                self.txtKode4.becomeFirstResponder()
            }
            if self.txtKode3.text!.characters.count == 0 {
                self.txtKode2.becomeFirstResponder()
            }
            self.txtKode3.resignFirstResponder()
        case self.txtKode4:
            if self.txtKode4.text!.characters.count > 0 {
                self.txtKode5.becomeFirstResponder()
            }
            if self.txtKode4.text!.characters.count == 0 {
                self.txtKode3.becomeFirstResponder()
            }
            self.txtKode4.resignFirstResponder()
        case self.txtKode5:
            if self.txtKode5.text!.characters.count > 0 {
                self.txtKode6.becomeFirstResponder()
            }
            if self.txtKode5.text!.characters.count == 0 {
                self.txtKode4.becomeFirstResponder()
            }
            self.txtKode5.resignFirstResponder()
        case self.txtKode6:
            if self.txtKode6.text!.characters.count > 0 {
                self.txtKode6.resignFirstResponder()
            }
            if self.txtKode6.text!.characters.count == 0 {
                self.txtKode5.becomeFirstResponder()
            }
            self.txtKode6.resignFirstResponder()
        default:
            print("Bukan Text Kode")
        }
    }
    
    @IBAction func btnKirimTapped(_ sender: UIButton) {
        if self.txtKode1.text! == "" || self.txtKode2.text! == "" || self.txtKode3.text! == "" || self.txtKode4.text! == "" || self.txtKode5.text! == "" || self.txtKode5.text! == "" {
            let alert = UIAlertController(title: "Info", message: "Please complete data", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.kirimOTP()
        }
    }
    
    func kirimOTP(){
        if self.from == 1 {
            self.otpRegister()
        } else {
            self.otpLogin()
        }
    }
    
    func otpRegister(){
        var kodeUser = ""
        let dataUser = try! Realm().objects(tb_user.self).first!
        kodeUser = dataUser.kode_user
        let kodeOtp = self.txtKode1.text! + self.txtKode2.text! + self.txtKode3.text! + self.txtKode4.text! + self.txtKode5.text! + self.txtKode6.text!        
        let url = "\(Helper.baseUrl)v1/users/confirm_and_login"
        let headers = [
            "x-kode-user" : kodeUser,
            "x-kode-otp" : kodeOtp,
            "x-client-id" : Helper.clientId,
            "x-device-id" : Helper.uid,
            "x-device-model" : Helper.deviceName,
            "x-app-version" : Helper.appVersion
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)

                    if jason["success"].stringValue == "false" {
                        self.showErrorMsg("Gagal", msg: jason["message"].stringValue)
                    }else{
                        let modelToken = tb_token()
                        modelToken.access_token = jason["access_token"].stringValue
                        modelToken.token_type = jason["token_type"].stringValue
                        modelToken.expires_in = jason["expires_in"].intValue
                        modelToken.refresh_token = jason["access_token"].stringValue
                        DBHelper.insert(modelToken)
                        self.performSegue(withIdentifier: "showRegisterBerhasil", sender: self)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    func otpLogin(){
        let kodeOtp = self.txtKode1.text! + self.txtKode2.text! + self.txtKode3.text! + self.txtKode4.text! + self.txtKode5.text! + self.txtKode6.text!
        let url = "\(Helper.baseUrl)v1/users/confirm_new_device"
        let headers = [
            "x-username" : self.username,
            "x-kode-otp" : kodeOtp,
            "x-client-id" : Helper.clientId,
            "x-device-id" : Helper.uid,
            "x-device-model" : Helper.deviceName,
            "x-app-version" : Helper.appVersion,
            "Accept" : "application/json"
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    let data = jason["data"]
                    print(jason)
                    
                    if jason["success"].stringValue == "false" {
                        self.showErrorMsg("Gagal", msg: jason["message"].stringValue)
                    }else{
                        let modelToken = tb_token()
                        modelToken.access_token = jason["access_token"].stringValue
                        modelToken.token_type = jason["token_type"].stringValue
                        modelToken.expires_in = jason["expires_in"].intValue
                        modelToken.refresh_token = jason["access_token"].stringValue
                        DBHelper.insert(modelToken)
                        let modelUser = tb_user()
                        modelUser.kode_user = data["kode_user"].stringValue
                        modelUser.email = data["email"].stringValue
                        modelUser.telepon = data["telepon"].stringValue
                        modelUser.nama_depan = data["nama_depan"].stringValue
                        modelUser.nama_belakang = data["nama_belakang"].stringValue
                        modelUser.jk = data["jk"].stringValue
                        modelUser.ktp = data["ktp"].stringValue
                        modelUser.role_id = data["role_id"].intValue
                        modelUser.role = data["role"].stringValue
                        modelUser.nama_role = data["nama_role"].stringValue
                        modelUser.saldo = data["saldo"].intValue
                        modelUser.fee = data["fee"].intValue
                        modelUser.poin = data["poin"].intValue
                        modelUser.seat = data["seat"].intValue
                        modelUser.kode_referal = data["kode_referal"].stringValue
                        DBHelper.insert(modelUser)
                        
                        let show = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showHome") as! TabbarHomeController
                        self.present(show, animated: true, completion: nil)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    @IBAction func btnKirimUlangKodeTapped(_ sender: UIButton) {
        self.resendOTP()
    }
    
    func resendOTP(){
        if self.from == 1 {
            self.resendOtpRegister()
        } else {
            self.resendOtpLogin()
        }
    }
    
    func resendOtpRegister(){
        let url = "\(Helper.baseUrl)v1/users/resend_otp"
        let headers = [
            "x-handphone" : self.username,
            "Accept" : "application/json"
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    if jason["success"].stringValue == "false" {
                        self.showErrorMsg("Gagal", msg: jason["message"].stringValue)
                    }else{
                        self.showErrorMsg("Berhasil", msg: jason["message"].stringValue)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    func resendOtpLogin(){
        let url = "\(Helper.baseUrl)v1/sessions/resend_otp"
        let headers = [
            "x-handphone" : self.username,
            "x-client-id" : Helper.clientId,
            "Accept" : "application/json"
        ]
        Alamofire.request(url, method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                if let jason = response.result.value {
                    let jason = JSON(jason)
                    print(jason)
                    if jason["success"].stringValue == "false" {
                        self.showErrorMsg("Gagal", msg: jason["message"].stringValue)
                    }else{
                        self.showErrorMsg("Berhasil", msg: jason["message"].stringValue)
                    }
                }else{
                    print("request gagal")
                    let alert = UIAlertController(title: "Error", message: "Terjadi kesalahan, silahkan coba beberapa saat lagi", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                    self.present(alert, animated: true, completion: nil)
                }
        }
    }
    
    func showErrorMsg(_ title: String, msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnKembaliTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        let info : NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        self.constBottom.constant = keyboardSize!.height
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.constBottom.constant = 0
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }

}
