//
//  WelcomeScreenController.swift
//  MitraAbutours
//
//  Created by Caryta on 8/29/17.
//  Copyright © 2017 PT. Amanah Bersama Ummat. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class WelcomeScreenController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var viewDotPage: [UIView]!
    @IBOutlet weak var btnDaftar: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    var pointScroll = [Int]() //e.g iPhone 7 [0, 375, 750, 1125]
    
    let swipeGestureLeft = UISwipeGestureRecognizer()
    let swipeGestureRight = UISwipeGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let widthScreen = UIScreen.main.bounds.size.width
        for i in 0...3 {
            let plus = Int(widthScreen) * i
            self.pointScroll.append(plus)
        }
        
        self.setSwipe()
        self.setDesign()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDesign(){
        let customColor = Helper.customColorGreen1.cgColor
        for viewDot in self.viewDotPage {
            viewDot.layer.cornerRadius = viewDot.frame.height / 2
            viewDot.layer.borderWidth = 1
            viewDot.layer.borderColor = customColor
        }
        self.btnDaftar.layer.cornerRadius = self.btnDaftar.frame.height / 2
    }
    
    func setSwipe(){
        self.swipeGestureLeft.direction = .left
        self.swipeGestureRight.direction = .right
        self.swipeGestureLeft.addTarget(self, action: #selector(self.handleSwipeLeft(_:)))
        self.swipeGestureRight.addTarget(self, action: #selector(self.handleSwipeRight(_:)))
        self.view.addGestureRecognizer(self.swipeGestureLeft)
        self.view.addGestureRecognizer(self.swipeGestureRight)
    }
    
    func handleSwipeLeft(_ gesture: UISwipeGestureRecognizer){
        if self.pageControl.currentPage < 3 {
            self.pageControl.currentPage += 1
            self.setCurrentData()
        }
    }
    
    func handleSwipeRight(_ gesture: UISwipeGestureRecognizer){
        if self.pageControl.currentPage != 0 {
            self.pageControl.currentPage -= 1
            self.setCurrentData()
        }
    }
    
    func setCurrentData(){
        let currentPage = self.pageControl.currentPage
        let pointX = self.pointScroll[currentPage]
        scrollView.setContentOffset(CGPoint(x: pointX, y: 0), animated: true)
    }
    
    @IBAction func btnDaftarTapped(_ sender: UIButton) {
        self.changeIntroData()
    }
    
    @IBAction func btnSkipTapped(_ sender: UIButton) {
        self.changeIntroData()
    }
    
    func changeIntroData(){
        let data = intro()
        data.intro = 1
        DBHelper.insert(data)
    }
}
